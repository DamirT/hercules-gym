DROP SCHEMA IF EXISTS gym;
CREATE SCHEMA gym DEFAULT CHARACTER SET utf8;
USE gym;

CREATE TABLE training (
	id BIGINT AUTO_INCREMENT,
	name VARCHAR(20),
	coach VARCHAR(20),
	description text(60) NOT NULL,
	price int(20) NOT NULL,
    duration int(20) NOT NULL,
    type_of_training varchar(20) not null,
    level_of_training varchar(25) not null,
	PRIMARY KEY(id)
);



INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training) VALUES ('Cardio', 'John Space', 'Cardio weight loss training', 1000, 120, 'Individual', 'Amateur');
INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training) VALUES ('Power lifting', 'Zak Hurshel', 'Power lifting to gain strength', 850, 90, 'Group', 'Medium');
INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training) VALUES ('Condition', 'Jay Cuttler', 'Trainig for improved fitness', 1200, 60, 'Group', 'Advanced');


CREATE TABLE user (
	id BIGINT AUTO_INCREMENT,
	user_name VARCHAR(20) not null,
	password VARCHAR(20) not null,
	email text(20) NOT NULL,
	name text(20) NOT NULL,
    surname text(40) NOT NULL,
    date_of_birth date NOT NULL,
    adress text(70) NOT NULL,
    phone_number VARCHAR(20) not null,
	user_role varchar(20) not null,
    date_of_registration datetime not null,
	PRIMARY KEY(id)
);

INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration) VALUES ('peki', 'peki', 'petar@gmail.com', 'Petar', 'Petrovic', '1988-12-01', 'Stevana Mokranjca 5, Nis', '0631234567', 'ADMIN', '2021-11-08 10:24');
INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration) VALUES ('aca', 'aca', 'aca@gmail.com', 'Aleksandar', 'Nikolic', '1995-10-03', 'Gogoljeva 6, Novi Sad', '0631234333', 'USER', '2021-03-010 11:00');
INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration) VALUES ('joca', 'joca', 'jova@gmail.com', 'Jova', 'Jovic', '1990-07-02', 'Jevrejska 5, Novi Sad', '0633333337', 'USER', '2022-01-08 10:00');




