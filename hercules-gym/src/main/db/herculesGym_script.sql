DROP SCHEMA IF EXISTS gym;
CREATE SCHEMA gym DEFAULT CHARACTER SET utf8;
USE gym;

CREATE TABLE training (
	id BIGINT AUTO_INCREMENT,
	name VARCHAR(70),
	coach VARCHAR(20),
	description text(150) NOT NULL,
	price double(11, 2) NOT NULL,
    duration int(20) NOT NULL,
    type_of_training varchar(20) not null,
    level_of_training varchar(25) not null,
	avg_assesment float,
	PRIMARY KEY(id)
);

INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training, avg_assesment) VALUES ('Cardio training', 'John Space', 'weight loss', 1000.00, 120, 'Individual', 'Amateur', 2.4);
INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training, avg_assesment) VALUES ('Power training', 'Zak Hurshel', 'Power lifting', 850.00, 90, 'Group', 'Medium', 3.4);
INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training, avg_assesment) VALUES ('Condition training', 'Jay Cuttler', 'improved fitness', 1200.00, 60, 'Group', 'Advanced', 4.3);
INSERT INTO training (name, coach, description, price, duration, type_of_training, level_of_training, avg_assesment) VALUES ('Fitness training', 'Jack Bond', 'training for improved fitness and condition', 900.00, 70, 'Group', 'Advanced', 4.5);


CREATE TABLE user (
	id BIGINT AUTO_INCREMENT,
	user_name VARCHAR(20) not null,
	password VARCHAR(20) not null,
	email text(20) NOT NULL,
	name text(20) NOT NULL,
    surname text(40) NOT NULL,
    date_of_birth date NOT NULL,
    adress text(70) NOT NULL,
    phone_number VARCHAR(20) not null,
	user_role varchar(20) not null,
    date_of_registration datetime not null,
    active boolean not null,
	PRIMARY KEY(id)
);

INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration, active) VALUES ('peki', 'peki', 'petar@gmail.com', 'Petar', 'Petrovic', '1988-12-01', 'Stevana Mokranjca 5, Nis', '0631234567', 'ADMIN', '2021-11-08 10:24', 1);
INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration, active) VALUES ('aca', 'aca', 'aca@gmail.com', 'Aleksandar', 'Nikolic', '1995-10-03', 'Gogoljeva 6, Novi Sad', '0631234333', 'USER', '2021-03-010 11:00', 1);
INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration, active) VALUES ('joca', 'joca', 'jova@gmail.com', 'Jova', 'Jovic', '1990-07-02', 'Jevrejska 5, Novi Sad', '0633333337', 'USER', '2022-01-08 10:00', 1);
INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration, active) VALUES ('maki', 'maki', 'maki@gmail.com', 'Marko', 'Markovic', '1995-010-05', 'Dositejeva 15, Novi Sad', '0632343243234', 'USER', '2022-11-08 10:15', 1);
INSERT INTO user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration, active) VALUES ('dzoni', 'dzoni', 'dzoni@gmail.com', 'Nikola', 'Nikolic', '1992-04-09', 'Rudarska 2, Bor', '0618298233782', 'USER', '2022-02-02 12:15', 1);

create table trainingType (
	id BIGINT AUTO_INCREMENT,
    title varchar(20) not null,
    detailDescription varchar(180) not null,
    primary key(id)
);

insert into trainingType(title, detailDescription) values ('Power lifting', 'Power lifting to gain strength');
insert into trainingType(title, detailDescription) values ('Condition', 'Trainig for improved fitness');
insert into trainingType(title, detailDescription) values ('Cardio', 'Cardio weight loss training');
insert into trainingType(title, detailDescription) values ('Yoga', 'Endurance training as well as peace of mind');

create table trainingTypeTraining (
	trainingTypeId bigint,
    trainingId bigint,
    primary key(trainingTypeId, trainingId),
    foreign key(trainingTypeId) references trainingType(id)
		on delete cascade,
	foreign key(trainingId) references training(id)
		on delete cascade
);

insert into trainingTypetraining(trainingId, trainingTypeId) values (1, 3);
insert into trainingTypetraining(trainingId, trainingTypeId) values (1, 1);
insert into trainingTypetraining(trainingId, trainingTypeId) values (2, 2);
insert into trainingTypetraining(trainingId, trainingTypeId) values (3, 4);
insert into trainingTypetraining(trainingId, trainingTypeId) values (4, 4);

create table hall (
	id BIGINT AUTO_INCREMENT,
    capacity int not null,
    hall_mark varchar(15) not null,
    occupied boolean not null,
    primary key(id)
);

insert into hall(capacity, hall_mark, occupied) values (5, '15a', 0);
insert into hall(capacity, hall_mark, occupied) values (4, '20b', 0);
insert into hall(capacity, hall_mark, occupied) values (3, '10', 0);
insert into hall(capacity, hall_mark, occupied) values (6, '7', 0);

create table appointment (
	id BIGINT AUTO_INCREMENT, 
    date_time_of_appoitment BIGINT not null,
    hallId BIGINT not null,
    trainingId BIGINT not null,
    foreign key(hallId) references hall(id)
		on delete cascade,
	foreign key(trainingId) references training(id)
		on delete cascade,
    primary key(id)
);

insert into appointment(hallId, trainingId, date_time_of_appoitment) values (1, 4, '1645866000000');
insert into appointment(hallId, trainingId, date_time_of_appoitment) values (1, 1, '1645830000000');
insert into appointment(hallId, trainingId, date_time_of_appoitment) values (2, 3, '1646031600000');
insert into appointment(hallId, trainingId, date_time_of_appoitment) values (3, 2, '1647686700000');


create table userAppointment (
	id bigint auto_increment,
	userId bigint,
    appointmentId bigint,
    date_and_time_of_order datetime not null,
    total_price_per_reservation double NOT NULL,
    primary key(id, userId, appointmentId),
    foreign key(userId) references user(id)
		on delete cascade,
	foreign key(appointmentId) references appointment(id)
		on delete cascade
);


create table userTraining (
	userId bigint,
    trainingId bigint,
    primary key(userId, trainingId),
    foreign key(userId) references user(id)
		on delete cascade,
	foreign key(trainingId) references training(id)
		on delete cascade
);

create table specialDateDiscount (
	id BIGINT AUTO_INCREMENT,
	special_date_discount date not null,
    discount_by_percentage int not null,
    primary key(id)
);

INSERT INTO specialDateDiscount (special_date_discount, discount_by_percentage) VALUES ('2020-12-01', 50);
INSERT INTO specialDateDiscount (special_date_discount, discount_by_percentage) VALUES ('2022-08-08', 40);

create table LoyaltyCard (
	id BIGINT AUTO_INCREMENT,
	userId bigint not null,
    points int not null,
    discount int not null,
    approved boolean not null,
    rejected boolean not null,
    primary key(id)
);

create table comment (
	id BIGINT AUTO_INCREMENT,
	text_comment text not null,
    assesment int not null,
    date_of_post date not null,
    userId bigint not null,
    trainingId bigint not null,
    approved boolean not null,
    rejected boolean not null,
    anonymous boolean not null,
    primary key(id)
);

