package com.ftn.herculesGym.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.TrainingTypeDAO;
import com.ftn.herculesGym.model.TrainingType;
import com.ftn.herculesGym.service.TrainingTypeService;

@Service
public class TrainingTypeServiceImpl implements TrainingTypeService{

	@Autowired
	private TrainingTypeDAO trainingTypeDAO;
	
	@Override
	public TrainingType findOne(Long id) {
		return trainingTypeDAO.findOne(id);
	}

	@Override
	public List<TrainingType> findAll() {
		return trainingTypeDAO.findAll();
	}

	@Override
	public List<TrainingType> findTrainigTypesByIds(Long[] ids) {
		List<TrainingType> results = new ArrayList<TrainingType>();
		for(Long id : ids) {
			TrainingType tt = trainingTypeDAO.findOne(id);
			results.add(tt);
		}
		return results;
	}

}
