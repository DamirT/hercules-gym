package com.ftn.herculesGym.service;

import java.sql.Date;
import java.util.List;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;

public interface TrainingService {

	Training findOne(Long id);
	
	List<Training> findAll();
	
	Training save(Training training);
	
	Training update(Training training);
	
	List<Training> findAllByNameSearch(String text);
	
	List<Training> findAllByCoachSearch(String text);
	
	List<Training> findAllByPriceSearch(String from, String until);
	
	List<Training> findByCapacityFulled();
	
	List<Training> findAllByExistsAppointment(List<Training> trainings);
	
	Training saveTrainingToUserWishList(User user, Training training);
	
	List<Training> findAllByUserWishList(User user);
	
	void deleteTrainingFromUserWishList(User user, Training training);
	
	List<Training> findAllByAppointment(Appointment appointemnt);
	
	Training findTrainingForComment(User user, Training training);
	
	List<Training> findAllByAppointmentsBeetwenDates(Date from, Date until);
}
