package com.ftn.herculesGym.service;

import java.util.List;

import com.ftn.herculesGym.model.LoyaltyCard;


public interface LoyaltyCardService {

	LoyaltyCard findone(Long id);
	
	List<LoyaltyCard> findAll();
	
	List<LoyaltyCard> findLoyaltyCardsOnRequest();
	
	LoyaltyCard save(LoyaltyCard loyaltyCard);
	
	LoyaltyCard update(LoyaltyCard loyaltyCard);
	
	LoyaltyCard findLoyaltyCardByUser(Long userId);
	
	void delete(Long id);
	
}
