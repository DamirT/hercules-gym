package com.ftn.herculesGym.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.UserDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public User findOne(Long id) {
		return userDAO.findOne(id);
	}

	@Override
	public User findOne(String userName, String password) {
		return userDAO.findOne(userName, password);
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	@Override
	public User save(User user) {
		userDAO.save(user);
		return user;
	}

	@Override
	public Boolean sameUserName(String username) {
		boolean sameUsername = false;
		for (User user : findAll()) {
			if (user.getUserName().equals(username)) {
				sameUsername = true;
			}
		}
		return sameUsername;
	}

	@Override
	public Boolean checkEmail(String email) {
		boolean validMail = false;
		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		if(matcher.matches()) {
			validMail = true;
		}
		return validMail;
	}

	@Override
	public List<User> findAllByUserNameSearch(String userNameSearch) {
		return userDAO.findAllByUserNameSearch(userNameSearch);
	}

	@Override
	public List<User> findAllByUserRoleSearch(String userRoleSearch) {
		return userDAO.findAllByUserRoleSearch(userRoleSearch);
	}

	@Override
	public User update(User user) {
		userDAO.updateUser(user);
		return user;
	}

	@Override
	public List<User> findAllByAppointmentId(Long appointmentId) {
		return userDAO.findAllByAppointmentId(appointmentId);
	}

	@Override
	public int countUserByAppointment(Long appointmentId) {
		List<User> usersByAppointment = new ArrayList<User>();
		List<User> allUsersByAppointmets = findAllByAppointmentId(appointmentId);
		for (int i = 0; i < allUsersByAppointmets.size(); i++) {
			User user = allUsersByAppointmets.get(i);
			usersByAppointment.add(user);
		}
		int counter = usersByAppointment.size() + 1;
		return counter;
	}
	

}
