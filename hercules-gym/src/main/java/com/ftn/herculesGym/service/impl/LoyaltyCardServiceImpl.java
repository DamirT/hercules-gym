package com.ftn.herculesGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.LoyaltyCardDAO;
import com.ftn.herculesGym.model.LoyaltyCard;
import com.ftn.herculesGym.service.LoyaltyCardService;

@Service
public class LoyaltyCardServiceImpl implements LoyaltyCardService{

	@Autowired
	private LoyaltyCardDAO loyaltyCardDAO;
	
	@Override
	public LoyaltyCard findone(Long id) {
		return loyaltyCardDAO.findOne(id);
	}

	@Override
	public List<LoyaltyCard> findAll() {
		return loyaltyCardDAO.findAll();
	}

	@Override
	public LoyaltyCard save(LoyaltyCard loyaltyCard) {
		loyaltyCardDAO.save(loyaltyCard);
		return loyaltyCard;
	}

	@Override
	public LoyaltyCard update(LoyaltyCard loyaltyCard) {
		loyaltyCardDAO.update(loyaltyCard);
		return loyaltyCard;
	}

	@Override
	public void delete(Long id) {
		LoyaltyCard loyaltyCard = findone(id);
		if (loyaltyCard != null) {
			loyaltyCardDAO.delete(id);
		}
	}

	@Override
	public LoyaltyCard findLoyaltyCardByUser(Long userId) {
		return loyaltyCardDAO.findLoyaltyCardByUser(userId);
	}

	@Override
	public List<LoyaltyCard> findLoyaltyCardsOnRequest() {
		return loyaltyCardDAO.findLoyaltyCardsOnRequest();
	}

	

}
