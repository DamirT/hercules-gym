package com.ftn.herculesGym.service;

import java.util.List;

import com.ftn.herculesGym.model.TrainingType;


public interface TrainingTypeService {

	TrainingType findOne(Long id);
	
	List<TrainingType> findAll();
	
	List<TrainingType> findTrainigTypesByIds(Long[] ids);
}
