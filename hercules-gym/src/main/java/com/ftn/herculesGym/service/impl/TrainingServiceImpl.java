package com.ftn.herculesGym.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.expression.Sets;

import com.ftn.herculesGym.dao.TrainingDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.HallService;
import com.ftn.herculesGym.service.TrainingService;
import com.ftn.herculesGym.service.UserService;

@Service
public class TrainingServiceImpl implements TrainingService{

	@Autowired
	private TrainingDAO trainingDAO;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired 
	private UserService userService;
	
	@Autowired
	private HallService hallService;
	
	@Override
	public Training findOne(Long id) {
		return trainingDAO.findOne(id);
	}

	@Override
	public List<Training> findAll() {
		return trainingDAO.findAll();
	}

	@Override
	public Training save(Training training) {
		trainingDAO.save(training);
		return training;
	}

	@Override
	public Training update(Training training) {
		trainingDAO.update(training);
		return training;
	}

	@Override
	public List<Training> findAllByNameSearch(String text) {
		return trainingDAO.findByNameSearch(text);
	}

	@Override
	public List<Training> findAllByCoachSearch(String text) {
		return trainingDAO.findByCoachSearch(text);
	}

	@Override
	public List<Training> findAllByPriceSearch(String from, String until) {
		return trainingDAO.findByPriceSearch(from, until);
	}

	@Override
	public List<Training> findByCapacityFulled() {
		Set<Training> targetSet = new HashSet<>();
		List<Training> allTrainings = findAll();
		for (int i = 0; i < allTrainings.size(); i++) {
			Training training = allTrainings.get(i);
			for (Appointment appointment : appointmentService.findAllByTrainings(training.getId())) {
				int counter = userService.countUserByAppointment(appointment.getId());
				Hall hall = hallService.findone(appointment.getHall().getId());
				if( counter <= hall.getCapacity()) {
					targetSet.add(training);
				} 
			}
		}
		List<Training> targetList = new ArrayList<>(targetSet);
		return targetList;
	}

	@Override
	public List<Training> findAllByExistsAppointment(List<Training> trainings) {
		Iterator<Training> iterator = trainings.iterator();
		while (iterator.hasNext()){
            Training t = iterator.next();
            List<Appointment> appointmentsByTraining = appointmentService.findAllByCapacityFulled(t.getId());;
            if(appointmentsByTraining.isEmpty()){   
                iterator.remove();                  
            }
        }
		return trainings;
	}

	@Override
	public Training saveTrainingToUserWishList(User user, Training training) {
		trainingDAO.saveTrainingToUserWishList(user, training);
		return training;
	}

	@Override
	public List<Training> findAllByUserWishList(User user) {
		return trainingDAO.findAllByUserWishList(user);
	}

	@Override
	public void deleteTrainingFromUserWishList(User user, Training training) {
		User u = userService.findOne(user.getId());
		Training t = findOne(training.getId());
		if (u != null && t != null) {
			trainingDAO.deleteTrainingFromUserWishList(u, t);
		}
	}

	@Override
	public List<Training> findAllByAppointment(Appointment appointemnt) {
		return trainingDAO.findAllByAppointment(appointemnt);
	}

	@Override
	public Training findTrainingForComment(User user, Training training) {
		return trainingDAO.findTrainingForComment(user, training);
	}

	@Override
	public List<Training> findAllByAppointmentsBeetwenDates(Date from, Date until) {
		return trainingDAO.findAllByAppointmentsBeetwenDates(from, until);
	}

}
