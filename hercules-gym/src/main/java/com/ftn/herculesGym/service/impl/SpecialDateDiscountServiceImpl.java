package com.ftn.herculesGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.SpecialDateDiscountDAO;
import com.ftn.herculesGym.model.SpecialDateDiscount;
import com.ftn.herculesGym.service.SpecialDateDiscountService;

@Service
public class SpecialDateDiscountServiceImpl implements SpecialDateDiscountService{

	@Autowired
	private SpecialDateDiscountDAO specialDateDiscountDAO;
	
	@Override
	public SpecialDateDiscount save(SpecialDateDiscount specialDateDiscount) {
		specialDateDiscountDAO.save(specialDateDiscount);
		return specialDateDiscount;
	}

	@Override
	public List<SpecialDateDiscount> findAll() {
		return specialDateDiscountDAO.findAll();
	}

}
