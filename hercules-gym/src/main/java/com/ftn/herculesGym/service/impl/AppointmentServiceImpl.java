package com.ftn.herculesGym.service.impl;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.AppointmentDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.HallService;
import com.ftn.herculesGym.service.UserService;

@Service
public class AppointmentServiceImpl implements AppointmentService{

	@Autowired
	private AppointmentDAO appointmentDAO;
	
	@Autowired 
	private UserService userService;
	
	@Autowired
	private HallService hallService;
	
	@Override
	public Appointment findone(Long id) {
		return appointmentDAO.findOne(id);
	}

	@Override
	public List<Appointment> findAll() {
		return appointmentDAO.findAll();
	}

	@Override
	public Appointment save(Appointment appointment) {
		appointmentDAO.save(appointment);
		return appointment;
	}
	
	@Override
	public Appointment update(Appointment appointment) {
		appointmentDAO.update(appointment);
		return appointment;
	}

	@Override
	public List<Appointment> findAllByTrainings(Long id) {
		return appointmentDAO.findAllByTraining(id);
	}

	@Override
	public List<Appointment> findAllByHall(Long id) {
		return appointmentDAO.findAllByHallId(id);
	}

	@Override
	public List<Appointment> findAllByCapacityFulled(Long trainingId) {
		Set<Appointment> setAppointmentsByCapacityFulled = new HashSet<>();
		for (Appointment appointment : findAllByTrainings(trainingId)) {
			int counter = userService.countUserByAppointment(appointment.getId());
			Hall hall = hallService.findone(appointment.getHall().getId());
			if(counter <= hall.getCapacity() && appointment.getDateAndTimeOfAppointment() > System.currentTimeMillis()) {
				setAppointmentsByCapacityFulled.add(appointment);
			} 
		}
		List<Appointment> appointmentsByCapacityFulled = new ArrayList<>(setAppointmentsByCapacityFulled);
		return appointmentsByCapacityFulled;
	}

	@Override
	public void saveAppointmentToUserShoppingCart(User user, List<Appointment> appointments,  LocalDateTime dateAndTimeOfOrder,  double totalPricePerReservation) {
		appointmentDAO.saveAppointmentsFromShoppinCart(user, appointments, dateAndTimeOfOrder, totalPricePerReservation);
	}

	@Override
	public List<Appointment> findAllAppointmentsByUser(User user) {
		return appointmentDAO.findAllAppointmentsByUser(user);
	}

	@Override
	public void deleteOrderedAppointment(User user, Appointment appointment) {
		User u = userService.findOne(user.getId());
		Appointment a = findone(appointment.getId());
		if (u != null && a != null) {
			appointmentDAO.deleteOrderedAppointment(u, a);
		}
	}
	
	@Override
	public List<Appointment> findAllAppointemntsByUserAndDateTime(User user, LocalDateTime ldt) {
		return appointmentDAO.findAllAppointemntsByUserAndDateTime(user, ldt);
	}

}
