package com.ftn.herculesGym.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.UserAppointmentDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;
import com.ftn.herculesGym.service.UserAppointmentService;

@Service
public class UserAppointmentImpl implements UserAppointmentService{

	@Autowired
	private UserAppointmentDAO userAppointmentDAO;
	
	

	@Override
	public List<UserAppointment> findAllUserAppointments(User user) {
		return userAppointmentDAO.findAllUserAppointments(user);
	}



	@Override
	public UserAppointment findOne(Long id) {
		return userAppointmentDAO.findOne(id);
	}



	@Override
	public void updateUserAppointmentWhenDelete(User user, LocalDateTime dateAndTimeOfReservation,
			double totalPricePerReservation) {
		userAppointmentDAO.updateUserappointemntWhenDelete(user, dateAndTimeOfReservation, totalPricePerReservation);
	}



	@Override
	public UserAppointment findUserAppointmentByAppointemntAndUser(Long appointmentId, User user) {
		return userAppointmentDAO.findUserAppointmentByAppointmentAndUser(appointmentId, user);
	}

}
