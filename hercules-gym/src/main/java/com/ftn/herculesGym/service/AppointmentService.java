package com.ftn.herculesGym.service;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;


public interface AppointmentService {

	Appointment findone(Long id);
	
	List<Appointment> findAll();
	
	Appointment update(Appointment appointment);
	
	Appointment save(Appointment appointment);
	
	List<Appointment> findAllByTrainings(Long id);
	
	List<Appointment> findAllByHall(Long id);
	
	List<Appointment> findAllByCapacityFulled(Long id);
	
	void saveAppointmentToUserShoppingCart(User user, List<Appointment> appointments, LocalDateTime dateAndTimeOfOrder,  double totalPricePerReservation);
	
	List<Appointment> findAllAppointmentsByUser(User user);
	
	void deleteOrderedAppointment(User user, Appointment appointment);
	
	List<Appointment> findAllAppointemntsByUserAndDateTime(User user, LocalDateTime ldt);
}
