package com.ftn.herculesGym.service;

import java.util.List;

import com.ftn.herculesGym.model.Hall;

public interface HallService {

	Hall findone(Long id);
	
	List<Hall> findAll();
	
	List<Hall> findAllByHallMark(String hallmark);
	
	Hall save(Hall hall);
	
	Hall update(Hall hall);
	
	Hall delete(Long id);
	
	Boolean sameHallMark(String hallMark);
	
	void setOccupiedForHall(List<Hall> halls);
}
