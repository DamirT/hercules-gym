package com.ftn.herculesGym.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;

@Service
public interface UserAppointmentService {
	
	List<UserAppointment> findAllUserAppointments(User user);
	
	UserAppointment findOne(Long id);
	
	void updateUserAppointmentWhenDelete(User user, LocalDateTime dateAndTimeOfReservation, double totalPricePerReservation);
	
	UserAppointment findUserAppointmentByAppointemntAndUser(Long appointmentId, User user);
}
