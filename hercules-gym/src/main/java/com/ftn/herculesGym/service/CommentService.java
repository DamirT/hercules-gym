package com.ftn.herculesGym.service;

import java.util.List;

import com.ftn.herculesGym.model.Comment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;

public interface CommentService {

	Comment findone(Long id);
	
	List<Comment> findAll();
	
	Comment save(Comment comment);
	
	Comment update(Comment comment);
	
	Comment delete(Long id);
	
	List<Comment> commentsForTraining(Training training);
	
	Comment findByUser(User user, Training training);
	
	List<Comment> findAllCommentsOnPending();
}
