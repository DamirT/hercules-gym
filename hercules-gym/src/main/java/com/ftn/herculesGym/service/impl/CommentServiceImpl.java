package com.ftn.herculesGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.CommentDAO;
import com.ftn.herculesGym.model.Comment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentDAO commentDAO;
	
	@Override
	public Comment findone(Long id) {
		return commentDAO.findOne(id);
	}

	@Override
	public List<Comment> findAll() {
		return commentDAO.findAll();
	}

	@Override
	public Comment save(Comment comment) {
		commentDAO.save(comment);
		return comment;
	}

	@Override
	public Comment update(Comment comment) {
		commentDAO.update(comment);
		return comment;
	}

	@Override
	public Comment delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Comment> commentsForTraining(Training training) {
		return commentDAO.commentsForTraining(training);
	}

	@Override
	public Comment findByUser(User user, Training training) {
		return commentDAO.findByUser(user, training);
	}

	@Override
	public List<Comment> findAllCommentsOnPending() {
		return commentDAO.findAllCommentsOnPending();
	}

}
