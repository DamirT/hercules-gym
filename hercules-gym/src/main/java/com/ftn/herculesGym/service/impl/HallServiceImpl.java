package com.ftn.herculesGym.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.HallDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.HallService;
import com.ftn.herculesGym.service.UserService;

@Service
public class HallServiceImpl implements HallService{

	@Autowired
	private HallDAO hallDAO;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserService userService;
	
	@Override
	public Hall findone(Long id) {
		return hallDAO.findOne(id);
	}

	@Override
	public List<Hall> findAll() {
		return hallDAO.findAll();
	}

	@Override
	public List<Hall> findAllByHallMark(String hallmark) {
		return hallDAO.findAllByHallMark(hallmark);
	}
	
	@Override
	public Hall save(Hall hall) {
		hallDAO.save(hall);
		return null;
	}

	@Override
	public Hall update(Hall hall) {
		hallDAO.update(hall);
		return hall;
	}

	@Override
	public Hall delete(Long id) {
		Hall hall = findone(id);
		if (hall != null) {
			hallDAO.delete(id);
		}
		return hall;
	}
	
	@Override
	public Boolean sameHallMark(String hallMark) {
		boolean sameHallMark = false;
		for (Hall hall : findAll()) {
			if (hall.getHallMark().equals(hallMark)) {
				sameHallMark = true;
			}
		}
		return sameHallMark;
	}

	@Override
	public void setOccupiedForHall(List<Hall> halls) {
		for (Hall hall  : halls) {
			for (Appointment a : appointmentService.findAllByHall(hall.getId())) {
				int counter = userService.countUserByAppointment(a.getId());
				if (a == null || counter == 1 || a.getDateAndTimeOfAppointment() < System.currentTimeMillis()) {
					hall.setOccupied(false);
					this.update(hall);
				}
				else {
					hall.setOccupied(true);
					this.update(hall);
					break;
				}
			}
			
		}
	}

}
