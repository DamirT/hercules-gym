package com.ftn.herculesGym.service;

import java.util.List;

import com.ftn.herculesGym.model.SpecialDateDiscount;

public interface SpecialDateDiscountService {

	SpecialDateDiscount save(SpecialDateDiscount specialDateDiscount);
	
	List<SpecialDateDiscount> findAll();
}
