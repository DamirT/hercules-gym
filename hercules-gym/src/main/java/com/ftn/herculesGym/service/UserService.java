package com.ftn.herculesGym.service;

import java.util.List;

import com.ftn.herculesGym.model.User;

public interface UserService {

	User findOne(Long id);
	
	User findOne(String userName, String password);
	
	List<User> findAllByUserNameSearch(String userNameSearch);
	
	List<User> findAllByUserRoleSearch(String userRoleSearch);
	
	List<User> findAll();
	
	User save(User user);
	
	Boolean sameUserName(String username);
	
	Boolean checkEmail(String email);
	
	User update(User user);
	
	List<User> findAllByAppointmentId(Long appointmentId);
	
	int countUserByAppointment(Long appointmentId);
}
