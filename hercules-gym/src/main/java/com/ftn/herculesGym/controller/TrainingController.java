package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Comment;
import com.ftn.herculesGym.model.LevelOfTraining;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.TrainingType;
import com.ftn.herculesGym.model.TypeOfTraining;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserRole;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.CommentService;
import com.ftn.herculesGym.service.TrainingService;
import com.ftn.herculesGym.service.TrainingTypeService;

@Controller
@RequestMapping("training")
public class TrainingController {

	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private TrainingTypeService trainingTypeService;
	
	@Autowired
	private CommentService commentService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	//private attribute to send an error message if user add training he already has in wish list
	private String error = "";
	
	//Here is keep Long trainingId when send redirect from addWishList to training/details 
	private Long holderIdTraining;
	
	@GetMapping
	public String getTrainings(HttpServletRequest request, ModelMap map, HttpSession session) {
		
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		
		if(loggedUser.getUserRole().equals(UserRole.ADMIN)) {
			List<Training> trainings = trainingService.findAll();
			map.put("trainings", trainings);
		}
		//logic for showing trainigs wich have appointment, only admin can see trainings without appointments
		if(loggedUser.getUserRole().equals(UserRole.USER)) {
			//In this list is only trainings wich doesn't have full capacity, for admin is showing all
			List<Training> trainings = trainingService.findByCapacityFulled();
//			List<Training> filteredTrainings = trainingService.findAllByExistsAppointment(trainings);
			map.put("trainings", trainings);
		}
		map.put("loggedUser", loggedUser);
		return "trainings";
	}
	
	@GetMapping("/details")
	public String getTraining(HttpServletRequest request, @RequestParam(required = false) Long id, ModelMap map) {
		User loggedUser = (User) request.getSession().getAttribute(UserController.LOGGED_USER_KEY);
		Training training = null;
		//if user adds training in wish list he already has, send message error with current trainingId
		if(!error.equals("")) {
			map.put("error", error);
			training = trainingService.findOne(holderIdTraining);
			error = "";
			holderIdTraining = null;
		 //else if user adds appointment in shopping cart he already has OR want to order appointment he alaready order, send message error with current trainingId
		}else if(!AppointmentController.error.equals("")) {
			String error = AppointmentController.error;
			Long holderIdTraining = AppointmentController.holderIdTraining;
			map.put("error", error);
			training = trainingService.findOne(holderIdTraining);
			AppointmentController.error = "";
			AppointmentController.holderIdTraining = null;
		} else {
			training = trainingService.findOne(id);
		}
		
		List<TrainingType> trainingsTypes = new ArrayList<TrainingType>();
		for (TrainingType tt : training.getListOfTrainingsType()) {
			trainingsTypes.add(tt);
		}
		List<Appointment> appointmentsByTraining = appointmentService.findAllByCapacityFulled(training.getId());
		
		//This is part for commenting training if user have or has appointment for him
		if (loggedUser.getUserRole().equals(UserRole.USER)) {
			boolean userCanCommentTraining = false;
			Training trainingForComment = trainingService.findTrainingForComment(loggedUser, training);
			if (trainingForComment != null) {
				userCanCommentTraining = true;
				map.put("trainingForComment", userCanCommentTraining);
			}
		}
		
		//This show all comments for training if has one
		List<Comment> commentsForTraining = commentService.commentsForTraining(training);
		if (commentsForTraining != null && !commentsForTraining.isEmpty()) {
			map.put("commentsForTraining", commentsForTraining);
		}
		//This is report for user whether comment is on pending or rejected, if it's approve, he will see him
		if (loggedUser.getUserRole().equals(UserRole.USER)) {
			Comment commentFromUser = commentService.findByUser(loggedUser, training);
			boolean pending = false;
			boolean rejected = false;
			boolean approved = false;
			if(commentFromUser != null) {
				if(commentFromUser.isApproved() == false && commentFromUser.isRejected() == false && commentFromUser.getTrainingId().equals(training.getId())) {
					pending = true;
				}
				if(commentFromUser.isApproved() == false && commentFromUser.isRejected() == true && commentFromUser.getId().equals(training.getId())) {
					rejected = true;
				}
				if(commentFromUser.isApproved() == true && commentFromUser.isRejected() == false && commentFromUser.getTrainingId().equals(training.getId())) {
					approved = true;
				}
			}
			map.put("pending", pending);
			map.put("rejected", rejected);
			map.put("approved", approved);
		}
		if(loggedUser.getUserRole().equals(UserRole.ADMIN)) {
			List<TrainingType> allTrainingTypesForUpdate = new ArrayList<TrainingType>();
			allTrainingTypesForUpdate = trainingTypeService.findAll();
			map.put("allTrainingTypesForUpdate", allTrainingTypesForUpdate);
		}
		
		map.put("loggedUser", loggedUser);
		map.put("trainingsTypes", trainingsTypes);
		map.put("training", training);
		map.put("appointmentsByTraining", appointmentsByTraining);
		
		
		
		return "trainingsDetail";
	}
	
	@PostMapping("/edit")
	public void updateTraining(@RequestParam String name, @RequestParam String coach, @RequestParam String description, 
							@RequestParam double price, @RequestParam int duration, @RequestParam String typeOfTraining, 
							@RequestParam String levelOfTraining, @RequestParam Long trainingId, 
							@RequestParam(name="trainingTypeId", required=false) Long[] trainingTypeIds, HttpServletResponse response) throws IOException {
		
		Training training = trainingService.findOne(trainingId); 
		
		if(training != null) {
			if (name != null && !name.trim().equals("")) {
				training.setName(name);
			}
			if (coach != null && !coach.trim().equals("")) {
				training.setCoach(coach);
			}
			if (description != null && !description.trim().equals("")) {
				training.setDescription(description);
			}
			if (price > 0) {
				training.setPrice(price);
			}
			if (duration > 0) {
				training.setDuration(duration);
			}
			TypeOfTraining type = TypeOfTraining.valueOf(typeOfTraining);
			training.setTypeOfTraining(type);
			
			LevelOfTraining level = LevelOfTraining.valueOf(levelOfTraining);
			training.setLevelOfTraining(level);
		}
		if(trainingTypeIds != null) {
			training.setListOfTrainingsType(trainingTypeService.findTrainigTypesByIds(trainingTypeIds));
		}
		trainingService.update(training);
		response.sendRedirect(bURL + "training");	
	}
	
	
	@GetMapping("/add")
	public String add(ModelMap map) {
		
		List<TrainingType> trainingsTypes = trainingTypeService.findAll();
		map.put("trainingsTypes", trainingsTypes);
		return "addTraining";
	}
	
	
	@PostMapping("/add")
	public void saveTraining(@RequestParam String name, @RequestParam String coach, @RequestParam String description, 
						@RequestParam double price, @RequestParam int duration, @RequestParam String typeOfTraining, 
						@RequestParam String levelOfTraining, @RequestParam(name="trainingTypeId", required=false) Long[] trainingTypeIds, HttpServletResponse response) throws IOException {
		
		Training training = new Training(name, coach, description, price, duration, TypeOfTraining.valueOf(typeOfTraining), LevelOfTraining.valueOf(levelOfTraining));
		training.setListOfTrainingsType(trainingTypeService.findTrainigTypesByIds(trainingTypeIds));
		
		trainingService.save(training);
		response.sendRedirect(bURL + "training");
	} 
	
	@PostMapping("/searchByName")
	public String searchByName(@RequestParam String search, ModelMap map, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		List<Training> trainings = trainingService.findAllByNameSearch(search);
		if(loggedUser.getUserRole().equals(UserRole.ADMIN)) {
			map.put("trainings", trainings);
		}
		if(loggedUser.getUserRole().equals(UserRole.USER)) {
			List<Training> filteredTrainings = trainingService.findAllByExistsAppointment(trainings);
			map.put("trainings", filteredTrainings);
		}
		map.put("loggedUser", loggedUser);
		return "trainings";
	}
	
	@PostMapping("/searchByCoach")
	public String searchByCoach(@RequestParam String search, ModelMap map,  HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		List<Training> trainings = trainingService.findAllByCoachSearch(search);
		if(loggedUser.getUserRole().equals(UserRole.ADMIN)) {
			map.put("trainings", trainings);
		}
		if(loggedUser.getUserRole().equals(UserRole.USER)) {
			List<Training> filteredTrainings = trainingService.findAllByExistsAppointment(trainings);
			map.put("trainings", filteredTrainings);
		}
		map.put("loggedUser", loggedUser);
		return "trainings";
	}
	
	@PostMapping("/searchByPrice")
	public String searchByPrice(@RequestParam String searchFrom, @RequestParam String searchUntil, ModelMap map, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		List<Training> trainings = trainingService.findAllByPriceSearch(searchFrom, searchUntil);
		if(loggedUser.getUserRole().equals(UserRole.ADMIN)) {
			map.put("trainings", trainings);
		}
		if(loggedUser.getUserRole().equals(UserRole.USER)) {
			List<Training> filteredTrainings = trainingService.findAllByExistsAppointment(trainings);
			map.put("trainings", filteredTrainings);
		}
		map.put("loggedUser", loggedUser);
		return "trainings";
	}
	
	@PostMapping("/addToWishList")
	public void addTrainingToUserWishList(@RequestParam Long trainingId, HttpSession session, HttpServletResponse response) throws IOException {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		Training training = trainingService.findOne(trainingId);
		
		//if user adds training in wish list he already has, send redirect to trainingDetail
		List<Training> trainingsByUserWishList = trainingService.findAllByUserWishList(loggedUser);
		if (trainingsByUserWishList != null) {
			for (Training t : trainingsByUserWishList) {
				if(t.getId() == training.getId()) {
					error = "You already added this training in wish list";
					holderIdTraining = trainingId;
					break;
				}
			}
		}
		if(!error.equals("")) {
			response.sendRedirect(bURL + "training/details");
		} else {
			trainingService.saveTrainingToUserWishList(loggedUser, training);
			response.sendRedirect(bURL + "training");
		}
	}
	
	@PostMapping("/deleteWishList")
	public void deleteTrainingFromUserWishList(@RequestParam Long trainingId, HttpServletResponse response, HttpSession session) throws IOException {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		Training training = trainingService.findOne(trainingId);
		trainingService.deleteTrainingFromUserWishList(loggedUser, training);
		response.sendRedirect(bURL + "user/userProfile");
	}
	
}
