package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.HallService;
import com.ftn.herculesGym.service.UserService;

@Controller
@RequestMapping("hall")
public class HallController {

	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private HallService hallService;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserService userService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public String getAllHalls(ModelMap map) {
		
		List<Hall> allHalls = hallService.findAll();
		hallService.setOccupiedForHall(allHalls);
		map.put("allHalls", allHalls);
		return "halls";
	}
	
	@GetMapping("/add")
	public String addHall() {
		return "addHall";
	}
	
	@PostMapping("/add")
	public String addHall(@RequestParam int capacity, @RequestParam String hallMark, HttpServletResponse response, ModelMap map) throws IOException {
		
		if(hallService.sameHallMark(hallMark)) {
			String error = "Hall mark already exist!";
			map.put("error", error);
			return "addHall";
		} 
		Hall hall = new Hall(capacity, hallMark);
		hallService.save(hall);
		response.sendRedirect(bURL + "hall");
		
		return null;
	}
	
	@GetMapping("/details")
	public String hallDetails(@RequestParam Long id, ModelMap map) {
		Hall hall = hallService.findone(id);
		map.put("hall", hall);
		return "hallDetails";
	}
	
	@PostMapping("/edit")
	public void hallEdit(@RequestParam Long hallId, @RequestParam int capacity, HttpServletResponse response) throws IOException {
		Hall hall = hallService.findone(hallId);
		hall.setCapacity(capacity);
		hallService.update(hall);
		response.sendRedirect(bURL + "hall");
	}
	
	@PostMapping("/delete")
	public void deleteHall(@RequestParam Long hallId, HttpServletResponse response) throws IOException {
		hallService.delete(hallId);
		response.sendRedirect(bURL + "hall");
	}
	
	@PostMapping("/searchByHallMark")
	public String searchByHallMark(ModelMap map, @RequestParam String hallMark) {
		List<Hall> allHalls = hallService.findAllByHallMark(hallMark);
		map.put("allHalls", allHalls);
		return "halls";
	}
	
}
