package com.ftn.herculesGym.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.CommentService;
import com.ftn.herculesGym.service.TrainingService;
import com.ftn.herculesGym.service.UserService;

@Controller
@RequestMapping("report")
public class ReportCotroller {

	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private TrainingService trainingService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public String reporting() {
		return "report";
	}
	
	@PostMapping("/getReport")
	public String getReposrt(@RequestParam String dateFromFront, 
							@RequestParam String dateUntilFront, ModelMap map) throws ParseException {
		
		Date dateFrom = Date.valueOf(dateFromFront); 
		Date dateUntil = Date.valueOf(dateUntilFront);
		
		List<Training> trainingsBySearchDates = trainingService.findAllByAppointmentsBeetwenDates(dateFrom, dateUntil);
		int numberOfOrder = trainingsBySearchDates.size();
		double totalPriceOfTrainings = 0;
		if(trainingsBySearchDates != null || trainingsBySearchDates.isEmpty()) {
			for (Training training : trainingsBySearchDates) {
				totalPriceOfTrainings += training.getPrice();
			}
		}
		Set<Training> filteredTrainings = new HashSet<Training>();
		List<Appointment> appointments = appointmentService.findAll();
		for (Appointment appointment : appointments) {
			filteredTrainings.addAll(trainingService.findAllByAppointment(appointment));
		}
		double totalSumOfAllOrderedTrainings = 0;
		for (Training t : filteredTrainings) {
			totalSumOfAllOrderedTrainings += t.getPrice();
		}
		int totalNumberOfOrderedTrainings = filteredTrainings.size();
		map.put("totalNumberOfOrderedTrainings", totalNumberOfOrderedTrainings);
		map.put("totalSumOfAllOrderedTrainings", totalSumOfAllOrderedTrainings);
		map.put("totalPriceOfTrainings", totalPriceOfTrainings);
		map.put("trainingsBySearchDates", trainingsBySearchDates);
		map.put("numberOfOrder", numberOfOrder);
		
		
		
		
		
		
		return "report";
	}
	
}
