package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;
import com.ftn.herculesGym.model.UserRole;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.TrainingService;
import com.ftn.herculesGym.service.UserAppointmentService;
import com.ftn.herculesGym.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {

	public static final String LOGGED_USER_KEY = "logged_user_key";
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserAppointmentService userAppointmentService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@PostMapping("/login")
	public String postLogin(@RequestParam String userName, @RequestParam String password, HttpServletResponse response, HttpSession session, ModelMap map) throws IOException {
		
		User loggedUser = userService.findOne(userName, password);
		String error = "";
		
		if (loggedUser == null) {
			error = "Defective credentials";
			map.put("error", error);
			error = "";
			return "index";
		}
		
		if(loggedUser.isActive() == false) {
			error = "You are deactivate from admin, can't login";
			map.put("error", error);
			error = "";
			return "index";
		}
		
		if (session.getAttribute(LOGGED_USER_KEY) != null) {
			error = "User is already logged, you need to logout first";
		}
		
		if(!error.equals("")) {
			map.put("error", error);
			return "login-loggedUser";
		}

		session.setAttribute(LOGGED_USER_KEY, loggedUser);
		response.sendRedirect(bURL + "training");
		return null;
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.removeAttribute(LOGGED_USER_KEY);
		session.invalidate();
		return "index";
	}
	
	@GetMapping("/registration")
	public String registration() {
		return "registration";
	}
	
	@PostMapping("/registration")
	public String registerUser(@RequestParam String name, @RequestParam String surname, @RequestParam String userName, 
							@RequestParam String password, @RequestParam String confirmPassword, @RequestParam String email, @RequestParam String adress, 
							@RequestParam String contact, @RequestParam String dateBirth, ModelMap map, HttpServletResponse response) {
		
		
		if(userService.checkEmail(email) == false) {
			String invalidEmailFormat = "Format of email si invalid";
			map.put("invalidEmailFormat", invalidEmailFormat);
			return "badRegistration";
		}
		
		if(!password.equals(confirmPassword)) {
			String badPassword = "Passwords must match";
			map.put("badPassword", badPassword);
			return "badRegistration";
		}
		if(name.equals("") || name.length() == 1) {
			String emptyName = "name is empty or smaller than 2 letters";
			map.put("emptyName", emptyName);
			return "badRegistration";
		}
		if(surname.equals("") || surname.length() < 3) {
			String emptySurname = "surname is empty or smaller than 3 letters";
			map.put("emptySurname", emptySurname);
			return "badRegistration";
		}
		if((userService.sameUserName(userName)) == true || userName.equals("")) {
			String emptyUserName = "username is empty or already exist";
			map.put("emptyUserName", emptyUserName);
			return "badRegistration";
		}
		if(adress.equals("") || adress.length() < 8) {
			String emptyAdress = "adress is empty or smaler than 8 characters";
			map.put("emptyAdress", emptyAdress);
			return "badRegistration";
		}
		if(contact.equals("") || contact.length() < 8) {
			String emptyContact = "contact must be at least 8 numbers";
			map.put("emptyContact", emptyContact);
			return "badRegistration";
		}
		if(dateBirth.equals("")) {
			String emptyDateBirth = "Must pick a date of birth";
			map.put("emptyDateBirth", emptyDateBirth);
			return "badRegistration";
		}
		
		LocalDateTime dateTime = LocalDateTime.now().plusHours(1);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		dateTime.format(format);
		
		User user = new User(name, surname, userName, password, email, adress, contact, LocalDate.parse(dateBirth));
		user.setUserRole(UserRole.USER);
		user.setDateOfRegistration(dateTime);
		userService.save(user);
		return "index";
	} 
	
	@GetMapping("/seeUserData")
	public String seeUserData(ModelMap map) {
		List<User> allUsers = userService.findAll();
		map.put("allUsers", allUsers);
		return "adminPageAllUsers";
	}
	
	@PostMapping("/searchByUserName")
	public String searchByUserName(@RequestParam String search, ModelMap map, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		List<User> allUsers = userService.findAllByUserNameSearch(search);
		map.put("allUsers", allUsers);
		map.put("loggedUser", loggedUser);
		return "adminPageAllUsers";
	}
	
	@PostMapping("/searchByUserRole")
	public String searchByUserRole(@RequestParam String userRole, ModelMap map, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		List<User> allUsers = userService.findAllByUserRoleSearch(userRole);
		map.put("allUsers", allUsers);
		map.put("loggedUser", loggedUser);
		return "adminPageAllUsers";
	}
	
	//This is page for Admin to see user details
	@GetMapping("/details")
	public String userDetails(ModelMap map, @RequestParam Long id, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		User user = userService.findOne(id);
		List<UserAppointment> orderedUserAppointments = userAppointmentService.findAllUserAppointments(user);
		double totalSumOfReservation = 0;
		for (UserAppointment ua : orderedUserAppointments) {
			totalSumOfReservation += ua.getTotalPricePerReservation();
		}
		map.put("user", user);
		map.put("loggedUser", loggedUser);
		map.put("orderedAppointmentsByUser", orderedUserAppointments);
		map.put("totalSumOfReservation", totalSumOfReservation);
		
		return "userDetails";
	}
	
	@PostMapping("/edit")
	public void editUserForAdmin(@RequestParam String userRole, @RequestParam Long userId, HttpServletResponse response) throws IOException {
		User user = userService.findOne(userId);
		user.setUserRole(UserRole.valueOf(userRole));
		userService.update(user);
		response.sendRedirect("seeUserData");
	}
	
	@PostMapping("/deactivateUser")
	public String deactivateUser(@RequestParam Long userId, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		User user = userService.findOne(userId);
		if(user.getUserRole() == UserRole.USER) {
			user.setActive(false);
			userService.update(user);
		} else {
			String error = "Can't deactivate admin!";
			map.put("error", error);
			map.put("user", user);
			map.put("loggedUser", loggedUser);
			return "userDetails";
		}
		response.sendRedirect("seeUserData");
		return null;
	}
	
	@PostMapping("/activateUser")
	public void activateUser(@RequestParam Long userId, HttpServletResponse response) throws IOException {
		User user = userService.findOne(userId);
		user.setActive(true);
		userService.update(user);
		response.sendRedirect("seeUserData");
	}
	
	//This is page for user profile
	@GetMapping("/userProfile")
	public String userProfile(ModelMap map, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		List<Training> trainingsByUserWishList = trainingService.findAllByUserWishList(loggedUser);
		List<UserAppointment> orderedUserAppointments = userAppointmentService.findAllUserAppointments(loggedUser);
		double totalSumOfReservation = 0;
		for (UserAppointment ua : orderedUserAppointments) {
			totalSumOfReservation += ua.getTotalPricePerReservation();
		}
		map.put("trainingsByUserWishList", trainingsByUserWishList);
		map.put("orderedAppointmentsByUser", orderedUserAppointments);
		map.put("loggedUser", loggedUser);
		map.put("totalSumOfReservation", totalSumOfReservation);
		return "userDetails";
	}
	
	@PostMapping("/editUser")
	public String editUser(@RequestParam Long userId, @RequestParam String name, @RequestParam String surname, @RequestParam String userName, 
						@RequestParam String password, @RequestParam String confirmPassword, @RequestParam String email, @RequestParam String adress, 
						@RequestParam String contact, @RequestParam String dateBirth, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException {
		
		User user = userService.findOne(userId);
		
		if(userService.checkEmail(email) == false) {
			String invalidEmailFormat = "Format of email si invalid";
			map.put("invalidEmailFormat", invalidEmailFormat);
			map.put("loggedUser", user);
			return "userDetails";
		}
		
		if(!password.equals(confirmPassword)) {
			String badPassword = "Passwords must match";
			map.put("badPassword", badPassword);
			map.put("loggedUser", user);
			return "userDetails";
		}
		if(name.equals("") || name.length() == 1) {
			String emptyName = "name is empty or smaller than 2 letters";
			map.put("emptyName", emptyName);
			map.put("loggedUser", user);
			return "userDetails";
		}
		if(surname.equals("") || surname.length() < 3) {
			String emptySurname = "surname is empty or smaller than 3 letters";
			map.put("emptySurname", emptySurname);
			map.put("loggedUser", user);
			return "userDetails";
		}
		if(((userService.sameUserName(userName)) == true && !userName.equals(user.getUserName()) || userName.equals(""))) {
			String emptyUserName = "username is empty or already exist";
			map.put("emptyUserName", emptyUserName);
			map.put("loggedUser", user);
			return "userDetails";
		}
		if(adress.equals("") || adress.length() < 8) {
			String emptyAdress = "adress is empty or smaler than 8 characters";
			map.put("emptyAdress", emptyAdress);
			map.put("loggedUser", user);
			return "userDetails";
		}
		if(contact.equals("") || contact.length() < 8) {
			String emptyContact = "contact must be at least 8 numbers";
			map.put("emptyContact", emptyContact);
			map.put("loggedUser", user);
			return "userDetails";
		}
		if(dateBirth.equals("")) {
			String emptyDateBirth = "Must pick a date of birth";
			map.put("emptyDateBirth", emptyDateBirth);
			map.put("loggedUser", user);
			return "userDetails";
		}
		
		user.setName(name);
		user.setSurname(surname);
		user.setUserName(userName);
		if (!password.equals("")) {
			user.setPassword(password);
		}
		user.setEmail(email);
		user.setAdress(adress);
		user.setPhoneNumber(contact);
		user.setDateBirth(LocalDate.parse(dateBirth));
		
		user = userService.update(user);
		session.setAttribute(LOGGED_USER_KEY, user);
		response.sendRedirect(bURL + "training");
		
		return null;
	}
	
}
