package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.SpecialDateDiscount;
import com.ftn.herculesGym.service.SpecialDateDiscountService;

@Controller
@RequestMapping("specialDateDiscount")
public class SpecialDateDiscountContoller {

	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private SpecialDateDiscountService specialDateDiscountService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	private String error = "";
	
	@GetMapping()
	public String specialDateDiscount(ModelMap map) {
		List<SpecialDateDiscount> specialDatesOfDiscountsInFutureDate = specialDateDiscountService.findAll();
		if(!error.equals("")) {
			map.put("error", error);
			error = "";
		}
		map.put("specialDatesOfDiscountsInFutureDate", specialDatesOfDiscountsInFutureDate);
		return "addSpecialDateDiscount";
	}
	
	@PostMapping("/addSpecialDateDiscount")
	public void addSpecialDateDiscount(@RequestParam String dateOfDiscount, @RequestParam int percentageOfDiscount,HttpServletResponse response) throws IOException {
		
		boolean validationForAddInPastTime = LocalDate.now().isAfter(LocalDate.parse(dateOfDiscount));
		boolean validationForExistDate = false;
		List<SpecialDateDiscount> specialDatesOfDiscountsInFutureDate = specialDateDiscountService.findAll();
		for  (SpecialDateDiscount sdd : specialDatesOfDiscountsInFutureDate) {
			if(sdd.getDateOfDiscount().equals(LocalDate.parse(dateOfDiscount))) {
				validationForExistDate = true;
				break;
			}
		}
		if(validationForAddInPastTime == true || validationForExistDate == true) {
			error = "You can't add discount in the past or the date that already added!";
		}
		LocalDate localDateoFDiscount = LocalDate.parse(dateOfDiscount);
		if(!error.equals("")) {
			response.sendRedirect(bURL + "specialDateDiscount");
		} else {
			SpecialDateDiscount specialDateDiscount = new SpecialDateDiscount(localDateoFDiscount, percentageOfDiscount);
			specialDateDiscountService.save(specialDateDiscount);
			response.sendRedirect(bURL + "specialDateDiscount");
		}
	}
	
}
