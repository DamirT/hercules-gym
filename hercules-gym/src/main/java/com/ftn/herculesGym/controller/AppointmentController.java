package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.LoyaltyCard;
import com.ftn.herculesGym.model.SpecialDateDiscount;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;
import com.ftn.herculesGym.model.UserRole;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.HallService;
import com.ftn.herculesGym.service.LoyaltyCardService;
import com.ftn.herculesGym.service.SpecialDateDiscountService;
import com.ftn.herculesGym.service.TrainingService;
import com.ftn.herculesGym.service.UserAppointmentService;
import com.ftn.herculesGym.service.UserService;
import com.mysql.cj.protocol.Message;

import ch.qos.logback.core.joran.conditional.ElseAction;

@Controller
@RequestMapping("appointment")
public class AppointmentController {

	public static final String APPOINTMENTS_FOR_ORDER = "appointments_for_order";
	
	//public  static String attribute to send an error message to training/details if user add appointment in shopping cart he already has
	public static String error = "";
	
	//Here is keep public static Long trainingId when send redirect from addAppointmentToUserCart to training/details 
	public static Long holderIdTraining;
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private HallService hallService;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserAppointmentService userAppointmentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SpecialDateDiscountService specialDateDiscountService;
	
	@Autowired
	private LoyaltyCardService loyaltyCardService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	
	
	@GetMapping("/add")
	public String addAppointment(ModelMap map) {
		List<Training> trainings = trainingService.findAll();
		List<Hall> halls = hallService.findAll();
		map.put("trainings", trainings);
		map.put("halls", halls);
		return "addAppointment";
	}
	
	@PostMapping("/add")
	public String saveAppointment(@RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate dateAppointment, 
								  @RequestParam @DateTimeFormat(iso=DateTimeFormat.ISO.TIME) LocalTime timeAppointment, 
								  @RequestParam Long trainingId, @RequestParam Long hallId, ModelMap map, HttpServletResponse response) throws IOException {
		
		LocalDateTime DateAndTime = LocalDateTime.of(dateAppointment, timeAppointment);
		ZonedDateTime zdt = DateAndTime.atZone(ZoneId.of("Europe/Belgrade"));
		Long dateAndTimeOfAppointment = zdt.toInstant().toEpochMilli();
		
		if(dateAndTimeOfAppointment < System.currentTimeMillis()) {
			String error = "You can't order appointment in the past time!";
			List<Training> trainings = trainingService.findAll();
			List<Hall> halls = hallService.findAll();
			map.put("trainings", trainings);
			map.put("halls", halls);
			map.put("error", error);
			return "addAppointment";
		}
		
		Training training = trainingService.findOne(trainingId);
		Hall hall = hallService.findone(hallId);
		
		Long minutesOfTrainingForAppointemnt = TimeUnit.MINUTES.toMillis(training.getDuration());
		Long totalDateAndTimeOfTrainingForAppointments = dateAndTimeOfAppointment + minutesOfTrainingForAppointemnt;
		
		Long minutesOfExistTrainingInAppointments = 0L;
		Long totalDateAndTimeOfExistTrainingForAppointments = 0L;
		for (Training t : trainingService.findAll()) {
			minutesOfExistTrainingInAppointments = TimeUnit.MINUTES.toMillis(t.getDuration());
			for (Appointment appointment : appointmentService.findAllByTrainings(t.getId())) {
				totalDateAndTimeOfExistTrainingForAppointments = appointment.getDateAndTimeOfAppointment() + minutesOfExistTrainingInAppointments;
				if (appointment.getHall().getId() == hall.getId() && appointment.getDateAndTimeOfAppointment() > System.currentTimeMillis()) {
					if(totalDateAndTimeOfTrainingForAppointments >= appointment.getDateAndTimeOfAppointment() 
						&& dateAndTimeOfAppointment <= totalDateAndTimeOfExistTrainingForAppointments 
						|| appointment.getDateAndTimeOfAppointment().equals(dateAndTimeOfAppointment)) {
						String error = "Hall is already taken in that time, pick another date or time!";
						List<Training> trainings = trainingService.findAll();
						List<Hall> halls = hallService.findAll();
						map.put("trainings", trainings);
						map.put("halls", halls);
						map.put("error", error);
						return "addAppointment";
					}
				}
			}
		}
		
		Appointment appointment = new Appointment(training, hall, dateAndTimeOfAppointment);
		appointmentService.save(appointment);
		
		response.sendRedirect(bURL + "training");
		return null;
	}
	
	@PostMapping("/addAppointmentToUserCart")
	public void addAppointmentToUserShoppingCart(@RequestParam Long appointmentId, HttpSession session, HttpServletResponse response) throws IOException {
		Appointment appointment = appointmentService.findone(appointmentId);
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		List<Appointment> appointmentsForOrder = (List<Appointment>) session.getAttribute(APPOINTMENTS_FOR_ORDER);
		if(appointmentsForOrder == null) {
			appointmentsForOrder = new ArrayList<Appointment>();
		}
		List<Appointment> orderedAppointmentsByUser = appointmentService.findAllAppointmentsByUser(loggedUser);
		//if user want to order appointment in the same period he ordered another appointment, send redirect to training/deatils with error message
		Long totalDateAndTimeOfWantedAppointmentForOrder = appointment.getDateAndTimeOfAppointment() + TimeUnit.MINUTES.toMillis(appointment.getTraining().getDuration());
		for (Appointment a : orderedAppointmentsByUser) {
			Long totalDateAndTimeOfOrderedAppointmentFromBase = a.getDateAndTimeOfAppointment() + TimeUnit.MINUTES.toMillis(a.getTraining().getDuration());
			if(totalDateAndTimeOfWantedAppointmentForOrder >= a.getDateAndTimeOfAppointment() 
				&& appointment.getDateAndTimeOfAppointment() <= totalDateAndTimeOfOrderedAppointmentFromBase 
				|| a.getDateAndTimeOfAppointment().equals(appointment.getDateAndTimeOfAppointment())) {
				error = "You already order appointment in this period!";
				holderIdTraining = appointment.getTraining().getId();
				break;
			}
		}
		//if user adds appointment in shopping cart he already has in Shopping cart, send redirect to trainingDetail
		if(!appointmentsForOrder.isEmpty()) {
			for (Appointment a : appointmentsForOrder) {
				Long totalDateAndTimeOfWantedAppointmentFromShoppCart = a.getDateAndTimeOfAppointment() + TimeUnit.MINUTES.toMillis(a.getTraining().getDuration());
				if(totalDateAndTimeOfWantedAppointmentForOrder >= a.getDateAndTimeOfAppointment()
					&& appointment.getDateAndTimeOfAppointment() <= totalDateAndTimeOfWantedAppointmentFromShoppCart
					|| a.getDateAndTimeOfAppointment().equals(appointment.getDateAndTimeOfAppointment())) {
					error = "You already have this appointment in shoppping cart or you want another appointment in the same period!";
					holderIdTraining = appointment.getTraining().getId();
					break;
				}
			}
		}
		if(error.equals("")) {
			appointmentsForOrder.add(appointment);
			session.setAttribute(APPOINTMENTS_FOR_ORDER, appointmentsForOrder);
			response.sendRedirect(bURL + "training");
		} else {
			response.sendRedirect(bURL + "training/details");
		}
	}
	
	@GetMapping("/shoppingCart")
	public String userShoppingCartOfAppointmets(HttpSession session, ModelMap map) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		List<Appointment> appointmentsForOrder = (List<Appointment>) session.getAttribute(APPOINTMENTS_FOR_ORDER);
		if(appointmentsForOrder == null) {
			appointmentsForOrder = new ArrayList<Appointment>();
		}
		//Here i check if today is special date for discount, if it is, than not showing input field for points discount
		LocalDate today = LocalDate.now();
		boolean todaySpecialDateDiscount = false;
		for (SpecialDateDiscount sdd : specialDateDiscountService.findAll()) {
			if (today.isEqual(sdd.getDateOfDiscount())) {
				todaySpecialDateDiscount = true;
				break;
			}
		}
		//Here check if user don't have loyalty card, not show field for input points
		LoyaltyCard loyaltyCard = loyaltyCardService.findLoyaltyCardByUser(loggedUser.getId());
		boolean userDontHaveLoyaltyCard = false;
		if(loyaltyCard == null) {
			userDontHaveLoyaltyCard = true;
		}
		map.put("userDontHaveLoyaltyCard", userDontHaveLoyaltyCard);
		map.put("todaySpecialDateDiscount", todaySpecialDateDiscount);
		map.put("appointmentsForOrder", appointmentsForOrder);
		return "shoppingCart";
	}
	
	@PostMapping("/deleteShoppingCart")
	public void deleteAppoinmentFromShoppingCart(@RequestParam Long appointmentId, HttpSession session, HttpServletResponse response) throws IOException {
		List<Appointment> appointmentsForOrder = (List<Appointment>) session.getAttribute(APPOINTMENTS_FOR_ORDER);
		for(Appointment appointment : appointmentsForOrder) {
			if(appointment.getId().equals(appointmentId)) {
				appointmentsForOrder.remove(appointment);
				break;
			}
		}
		response.sendRedirect(bURL + "appointment/shoppingCart"); 
	}
	
	@PostMapping("/saveAllFromShoppinCart")
	public String saveAllAppointmentsFromShoppingCart(HttpSession session, @RequestParam(required = false) Integer pointsNumber, HttpServletResponse response, ModelMap map) throws IOException {
		List<Appointment> appointmentsForOrder = (List<Appointment>) session.getAttribute(APPOINTMENTS_FOR_ORDER);
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		String message = "";
		LocalDateTime dateAndTimeOfOrder = LocalDateTime.now().minusHours(1);
		LocalDate dateOfOrder = LocalDate.now();

		Set<Training> trainingsByAppointments = new HashSet<Training>();
		//This is total price without any discount
		double totalPricePerReservation = 0;
		for(Appointment a : appointmentsForOrder) {
			trainingsByAppointments.add(a.getTraining());
		}
		for (Training t : trainingsByAppointments) {
			totalPricePerReservation += t.getPrice();
		}
		//---------------------------------
		//This is total  price with special day discount
		double totalPriceWithSpecialDiscount = 0;
		boolean discount = false;
		int percentageForMessage = 0;
		for (SpecialDateDiscount sdd : specialDateDiscountService.findAll()) {
			if (dateOfOrder.isEqual(sdd.getDateOfDiscount())) {
				double discountPerReservation = (totalPricePerReservation * sdd.getPercentage()) / 100;
				totalPriceWithSpecialDiscount = totalPricePerReservation - discountPerReservation;
				percentageForMessage = sdd.getPercentage();
				discount = true;
				break;
			}
		}
		//-----------------------------------------------
		//This is total price  with regular points discount
		boolean deficitPoints = false;
		double totalPriceWithRegularPointsDiscount = 0;
		LoyaltyCard loyaltyCard = loyaltyCardService.findLoyaltyCardByUser(loggedUser.getId());
		if(loyaltyCard != null) {
			if (pointsNumber != null) {
				double discountPerReservation = (totalPricePerReservation * (pointsNumber * 5)) / 100;
				totalPriceWithRegularPointsDiscount = totalPricePerReservation - discountPerReservation;
				if((loyaltyCard.getPoints() - pointsNumber) < 0) {
					deficitPoints = true;
				}
			}
		}
		
		if (discount) {
			appointmentService.saveAppointmentToUserShoppingCart(loggedUser, appointmentsForOrder, dateAndTimeOfOrder, totalPriceWithSpecialDiscount);
			session.setAttribute(APPOINTMENTS_FOR_ORDER, new ArrayList<Appointment>());
			message = "Congratulations, you have won a " + percentageForMessage + "% discount on this reservation";
			map.put("message", message);
			return "shoppingCart";
		} else if (deficitPoints) {
			message = "You are insert more points that you have!";
			map.put("message", message);
			return "shoppingCart";
		} else if (pointsNumber != null) {
			loyaltyCard.setPoints(loyaltyCard.getPoints() - pointsNumber);
			loyaltyCardService.update(loyaltyCard);
			appointmentService.saveAppointmentToUserShoppingCart(loggedUser, appointmentsForOrder, dateAndTimeOfOrder, totalPriceWithRegularPointsDiscount);
			session.setAttribute(APPOINTMENTS_FOR_ORDER, new ArrayList<Appointment>());
			response.sendRedirect(bURL + "appointment/shoppingCart");
			return null;
		} else {
			if (loyaltyCard != null) {
				int pointsForLoyaltyCard = (int )totalPricePerReservation / 500;
				loyaltyCard.setPoints(loyaltyCard.getPoints() + pointsForLoyaltyCard);
				loyaltyCardService.update(loyaltyCard);
			}
			appointmentService.saveAppointmentToUserShoppingCart(loggedUser, appointmentsForOrder, dateAndTimeOfOrder, totalPricePerReservation);
			session.setAttribute(APPOINTMENTS_FOR_ORDER, new ArrayList<Appointment>());
			response.sendRedirect(bURL + "appointment/shoppingCart");
			return null;
		}
	}
	
	@PostMapping("/deleteOrderedAppointment")
	public String deleteOrderedAppointment(@RequestParam Long appointmentId, HttpServletResponse response, HttpSession session, ModelMap map) {
		Appointment appointment = appointmentService.findone(appointmentId);
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		String error = "";
		if(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(24) > appointment.getDateAndTimeOfAppointment()) {
			error = "you can't cancel an appointment because there are less than 24 hours";
		}
		if(error.equals("")) {
			UserAppointment userAppointment = userAppointmentService.findUserAppointmentByAppointemntAndUser(appointmentId, loggedUser);
			List<Appointment> appointemtsByUserOrder = appointmentService.findAllAppointemntsByUserAndDateTime(loggedUser, userAppointment.getDateAndTimeOfOrder());
			
			Set<Training> trainingsByAppointments = new HashSet<Training>();
			for(Appointment a : appointemtsByUserOrder) {
				trainingsByAppointments.add(a.getTraining());
			}
			double percentageDividedPerReservation = 0;
			double updatedTotalPriceAfterDeleteWithDiscount = 0;
			for (SpecialDateDiscount sdd : specialDateDiscountService.findAll()) {
				if (userAppointment.getDateAndTimeOfOrder().toLocalDate().isEqual(sdd.getDateOfDiscount())) {
					if(!appointemtsByUserOrder.isEmpty()) {
						percentageDividedPerReservation = sdd.getPercentage() / appointemtsByUserOrder.size();
						for (Training t : trainingsByAppointments) {
							double pricePerPercentageOfOneTraining = t.getPrice() * percentageDividedPerReservation / 100;
							updatedTotalPriceAfterDeleteWithDiscount += t.getPrice() - pricePerPercentageOfOneTraining; 
						}
					}
				}
			}
			if(updatedTotalPriceAfterDeleteWithDiscount == 0) {
				double totalPricePerReservation = userAppointment.getTotalPricePerReservation() - appointment.getTraining().getPrice();
				userAppointmentService.updateUserAppointmentWhenDelete(loggedUser, userAppointment.getDateAndTimeOfOrder().minusHours(1), totalPricePerReservation);
			} 
			if(updatedTotalPriceAfterDeleteWithDiscount > 0) {
				userAppointmentService.updateUserAppointmentWhenDelete(loggedUser, userAppointment.getDateAndTimeOfOrder().minusHours(1), updatedTotalPriceAfterDeleteWithDiscount - appointment.getTraining().getPrice());
			}
			
			appointmentService.deleteOrderedAppointment(loggedUser, appointment);
			List<Training> trainingsByUserWishList = trainingService.findAllByUserWishList(loggedUser);
			List<UserAppointment> orderedUserAppointments = userAppointmentService.findAllUserAppointments(loggedUser);
			double totalSumOfReservation = 0;
			for (UserAppointment ua : orderedUserAppointments) {
				totalSumOfReservation += ua.getTotalPricePerReservation();
			}
			map.put("trainingsByUserWishList", trainingsByUserWishList);
			map.put("orderedAppointmentsByUser", orderedUserAppointments);
			map.put("loggedUser", loggedUser);
			map.put("totalSumOfReservation", totalSumOfReservation);
			
			return "userDetails";
		} else {
			List<Training> trainingsByUserWishList = trainingService.findAllByUserWishList(loggedUser);
			List<Appointment> orderedAppointmentsByUser = appointmentService.findAllAppointmentsByUser(loggedUser);
			map.put("trainingsByUserWishList", trainingsByUserWishList);
			map.put("orderedAppointmentsByUser", orderedAppointmentsByUser);
			map.put("loggedUser", loggedUser);
			map.put("error", error);
			
			return "userDetails";
		}
	}
	
	@GetMapping("seeReservation")
	public String seeDetailOfOrderedAppointment(@RequestParam Long id, @RequestParam(required = false) Long userId, ModelMap map, HttpSession session) {
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		UserAppointment userAppointment = userAppointmentService.findOne(id);
		if (loggedUser.getUserRole().equals(UserRole.ADMIN)) {
			User user = userService.findOne(userId);
			List<Appointment> appointemtsByUserOrder = appointmentService.findAllAppointemntsByUserAndDateTime(user, userAppointment.getDateAndTimeOfOrder());
			map.put("appointemtsByUserOrder", appointemtsByUserOrder);
		} else {
			List<Appointment> appointemtsByUserOrder = appointmentService.findAllAppointemntsByUserAndDateTime(loggedUser, userAppointment.getDateAndTimeOfOrder());
			map.put("appointemtsByUserOrder", appointemtsByUserOrder);
		}
		map.put("loggedUser", loggedUser);
		return "appointmentReservation";
	}
	
}
