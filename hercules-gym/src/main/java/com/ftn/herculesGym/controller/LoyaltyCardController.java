package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.LoyaltyCard;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserRole;
import com.ftn.herculesGym.service.LoyaltyCardService;
import com.ftn.herculesGym.service.UserService;

@Controller
@RequestMapping("loyaltyCard")
public class LoyaltyCardController {

	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LoyaltyCardService loyaltyCardService;;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public String loyaltyCard(ModelMap map, HttpSession session) {
		
		User loggedUser = (User) session.getAttribute(UserController.LOGGED_USER_KEY);
		
		if(loggedUser.getUserRole().equals(UserRole.USER)) {
			LoyaltyCard loyaltyCard = loyaltyCardService.findLoyaltyCardByUser(loggedUser.getId());
			boolean cardIsNull = false;
			boolean pending = false;
			boolean approved = false;
			boolean rejected = false;
			
			if(loyaltyCard == null) {
				cardIsNull = true;
				map.put("cardIsNull", cardIsNull);
				map.put("loggedUser", loggedUser);
				return "loyaltyCardUser";
			} 
			if (loyaltyCard.isApproved() == false && loyaltyCard.isRejected() == false) {
				pending = true;
			}
			if (loyaltyCard.isApproved() == true && loyaltyCard.isRejected() == false) {
				approved = true;
			}
			if (loyaltyCard.isApproved() == false && loyaltyCard.isRejected() == true) {
				rejected = true;
			}
			
			map.put("pending", pending);
			map.put("approved", approved);
			map.put("rejected", rejected);
			map.put("loyaltyCard", loyaltyCard);
			map.put("loggedUser", loggedUser);
			return "loyaltyCardUser";
		} else {
			List<LoyaltyCard> allLoyaltyCardsByUsersOnRequest = loyaltyCardService.findLoyaltyCardsOnRequest();
			if (allLoyaltyCardsByUsersOnRequest == null) {
				String message = "there are no pending requests!";
				map.put("message", message);
				return "loyaltyCardAdmin";
			} 
			if (allLoyaltyCardsByUsersOnRequest != null && !allLoyaltyCardsByUsersOnRequest.isEmpty()) {
				List<User> usersWhoPendingOnRequest = new ArrayList<User>();
				for (LoyaltyCard loyaltyCard : allLoyaltyCardsByUsersOnRequest) {
					if(loyaltyCard.isApproved() == false && loyaltyCard.isRejected() == false) {
						usersWhoPendingOnRequest.add(userService.findOne(loyaltyCard.getUserId()));
					}
				}
				map.put("usersWhoPendingOnRequest", usersWhoPendingOnRequest);
				return "loyaltyCardAdmin";
			}
			
		}
		return null;
	}
	
	@PostMapping("/RequestForLoyaltyCard")
	public void requestForLoyaltyCardByUser(@RequestParam Long userId, HttpServletResponse response) throws IOException {
		
		LoyaltyCard loyaltyCardByUser = loyaltyCardService.findLoyaltyCardByUser(userId);
		if (loyaltyCardByUser != null && loyaltyCardByUser.isRejected() == true) {
			loyaltyCardByUser.setRejected(false);
			loyaltyCardService.update(loyaltyCardByUser);
			response.sendRedirect(bURL + "training");
		} else {
			LoyaltyCard loyaltyCard = new LoyaltyCard(userId, 0, 0, false, false);
			loyaltyCardService.save(loyaltyCard);
			response.sendRedirect(bURL + "training");
		}
	}
	
	@PostMapping("/approveRequestLoyaltyCard")
	public void approveRequestForLoyaltyCard(@RequestParam Long userId, HttpServletResponse response) throws IOException {
		
		LoyaltyCard loyaltyCard = loyaltyCardService.findLoyaltyCardByUser(userId);
		if (loyaltyCard != null) {
			loyaltyCard.setApproved(true);
			loyaltyCard.setPoints(10);
			loyaltyCardService.update(loyaltyCard);
			response.sendRedirect(bURL + "loyaltyCard");
		}
	}
	
	@PostMapping("/rejectRequestLoyaltyCard")
	public void rejectRequestForLoyaltyCard(@RequestParam Long userId, HttpServletResponse response) throws IOException {
		
		LoyaltyCard loyaltyCard = loyaltyCardService.findLoyaltyCardByUser(userId);
		if (loyaltyCard != null) {
			loyaltyCard.setRejected(true);
			loyaltyCardService.update(loyaltyCard);
			response.sendRedirect(bURL + "loyaltyCard");
		}
	}
	
	
}
