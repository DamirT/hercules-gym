package com.ftn.herculesGym.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.herculesGym.model.Comment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.service.AppointmentService;
import com.ftn.herculesGym.service.CommentService;
import com.ftn.herculesGym.service.TrainingService;
import com.ftn.herculesGym.service.UserService;

@Controller
@RequestMapping("comment")
public class CommentController {

	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private TrainingService trainingService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@PostMapping("/addComent")
	public void saveCommentOnTraining(@RequestParam(required = false) boolean anonymous, @RequestParam String textComment, 
										@RequestParam int assesment, @RequestParam Long trainingId ,HttpServletResponse response, 
										HttpSession session, HttpServletRequest request) throws IOException {
		User loggedUser = (User) request.getSession().getAttribute(UserController.LOGGED_USER_KEY);
		LocalDate dateNow = LocalDate.now();
		Training training = trainingService.findOne(trainingId);
		
		Comment comment = null;
		if(training != null) {
			comment = commentService.findByUser(loggedUser, training);
			if(comment != null) {
				comment.setApproved(false);
				comment.setRejected(false);
				comment.setTextComment(textComment);
				comment.setAssesment(assesment);
				comment.setDateOfPostedComment(dateNow);
				comment.setAnonymous(anonymous);
				commentService.update(comment);
			}
		}
		if (comment == null) {
			comment = new Comment(textComment, assesment, dateNow, loggedUser, trainingId, false, false, anonymous);
			commentService.save(comment);
		}
		response.sendRedirect(bURL + "training"); 
	}
	
	@GetMapping("/commentsOnPending")
	public String commentsOnPending(ModelMap map) {
		
		List<Comment> commentsOnPending = commentService.findAllCommentsOnPending();
		map.put("commentsOnPending", commentsOnPending);
		return "commentsAdmin";
	}
	
	@PostMapping("/approveComment")
	public void approveComment(@RequestParam Long commentId, HttpServletResponse response) throws IOException {
		Comment comment = commentService.findone(commentId);
		if (comment != null) {
			comment.setApproved(true);
			comment.setRejected(false);
			commentService.update(comment);
		}
		
		Training training = trainingService.findOne(comment.getTrainingId());
		List<Comment> comments = commentService.commentsForTraining(training);
		float totalSumOfAssesments = 0;
		float avgAssesment = 0;
		if ( comments != null) {
			for (Comment com : comments) {
				totalSumOfAssesments += com.getAssesment();
			}
			avgAssesment = totalSumOfAssesments / comments.size();
			training.setAvgAssesment(avgAssesment);
		} else {
			training.setAvgAssesment(comment.getAssesment());
		}
		trainingService.update(training);
		response.sendRedirect(bURL + "comment/commentsOnPending");
	}
	
	@PostMapping("/rejectComment")
	public void rejectComment(@RequestParam Long commentId, HttpServletResponse response) throws IOException {
		Comment comment = commentService.findone(commentId);
		comment.setApproved(false);
		comment.setRejected(true);
		
		commentService.update(comment);
		response.sendRedirect(bURL + "comment/commentsOnPending");
	}
}
