package com.ftn.herculesGym.dao;

import java.util.List;

import com.ftn.herculesGym.model.User;


public interface UserDAO {

	public User findOne(Long id);
	
	public User findOne(String userName, String password);
	
	public List<User> findAll();
	
	public int save(User user);
	
	public List<User> findAllByUserNameSearch(String userName);
	
	public List<User> findAllByUserRoleSearch(String userRole);

	public int updateUser(User user);
	
	List<User> findAllByAppointmentId(Long appointmentd);
}
