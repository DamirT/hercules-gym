package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.CommentDAO;
import com.ftn.herculesGym.dao.UserDAO;
import com.ftn.herculesGym.model.Comment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;

@Repository
public class CommentDAOImpl implements CommentDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UserDAO userDAO;

	private class CommentRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Comment> comments = new LinkedHashMap<Long, Comment>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String textComment = rs.getString(index++);
			int aseesment = rs.getInt(index++);
			LocalDate dateOfPost = rs.getTimestamp(index++).toLocalDateTime().toLocalDate();
			Long userId = rs.getLong(index++);
			Long trainingId = rs.getLong(index++);
			boolean approved = rs.getBoolean(index++);
			boolean rejected = rs.getBoolean(index++);
			boolean anonymous = rs.getBoolean(index++);
			
			User user = userDAO.findOne(userId);
			Comment comment = comments.get(id);
			if(comment == null) {
				comment = new Comment(id, textComment, aseesment, dateOfPost, user, trainingId, approved, rejected, anonymous);
				if(comment != null) {
					comments.put(id, comment);
				}
			}
		}
		public List<Comment> getComments() {
			if(comments == null || comments.isEmpty()) {
				return null;
			} else {
				return new ArrayList<Comment>(comments.values());
			}
		}
		
	}
	
	@Override
	public Comment findOne(Long id) {
		String sql = "SELECT * FROM comment where id = ?";
		CommentRowCallBackHandler rowCallBackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		return rowCallBackHandler.getComments().get(0);
	}

	@Override
	public List<Comment> findAll() {
		String sql = "SELECT * from comment";
		CommentRowCallBackHandler rowCallBackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getComments();
	}

	@Override
	public int save(Comment comment) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into comment (text_comment, assesment, date_of_post, userId, trainingId, approved, rejected, anonymous) values (?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, comment.getTextComment());
				preparedStatement.setInt(index++, comment.getAssesment());
				preparedStatement.setDate(index++, Date.valueOf(comment.getDateOfPostedComment()));
				preparedStatement.setLong(index++, comment.getUser().getId());
				preparedStatement.setLong(index++, comment.getTrainingId());
				preparedStatement.setBoolean(index++, comment.isApproved());
				preparedStatement.setBoolean(index++, comment.isRejected());
				preparedStatement.setBoolean(index++, comment.isAnonymous());

				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public int update(Comment comment) {
		String sql = "update comment set text_comment = ?, assesment = ?, date_of_post = ?, userId = ?, trainingId = ?,"
				   + " approved = ?, rejected = ?, anonymous = ? where id = ?";
		boolean succses = jdbcTemplate.update(sql, comment.getTextComment(), comment.getAssesment(), comment.getDateOfPostedComment(),
												comment.getUser().getId(), comment.getTrainingId(), comment.isApproved(), 
												comment.isRejected(), comment.isAnonymous(), comment.getId()) == 1;
		return succses?1:0;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Comment> commentsForTraining(Training training) {
		String sql = "SELECT * from comment where trainingId = ? and approved = true and rejected = false";
		CommentRowCallBackHandler rowCallBackHandler = new CommentRowCallBackHandler();
		if (training != null) {
			jdbcTemplate.query(sql, rowCallBackHandler, training.getId());
		}
		if(rowCallBackHandler.getComments() == null) {
			return null;
		} else {
			return rowCallBackHandler.getComments();
		}
	}

	@Override
	public Comment findByUser(User user, Training training) {
		String sql = "SELECT * FROM comment where userId = ? and trainingId = ?";
		CommentRowCallBackHandler rowCallBackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, user.getId(), training.getId());
		if(rowCallBackHandler.getComments() == null) {
			return null;
		} else {
			return rowCallBackHandler.getComments().get(0);
		}
	}

	@Override
	public List<Comment> findAllCommentsOnPending() {
		String sql = "SELECT * from comment where approved = false and rejected = false";
		CommentRowCallBackHandler rowCallBackHandler = new CommentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getComments();
	}

}
