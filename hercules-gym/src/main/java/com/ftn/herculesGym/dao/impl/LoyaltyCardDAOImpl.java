package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.ftn.herculesGym.dao.LoyaltyCardDAO;
import com.ftn.herculesGym.model.LoyaltyCard;

@Repository
public class LoyaltyCardDAOImpl implements LoyaltyCardDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class LoyaltyCardRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, LoyaltyCard> loyaltyCards = new LinkedHashMap<Long, LoyaltyCard>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			Long userId = rs.getLong(index++);
			int points = rs.getInt(index++);
			int discount = rs.getInt(index++);
			boolean approved = rs.getBoolean(index++);
			boolean rejected = rs.getBoolean(index++);
			
			LoyaltyCard loyaltyCard = loyaltyCards.get(id);
			if(loyaltyCard == null) {
				loyaltyCard = new LoyaltyCard(id, userId, points, discount, approved, rejected);
				if (loyaltyCard != null) {
					loyaltyCards.put(id, loyaltyCard);
				}
				
			}
		}
		public List<LoyaltyCard> getLoyalityCards() {
			if(loyaltyCards == null || loyaltyCards.isEmpty()) {
				return null;
			} else {
				return new ArrayList<LoyaltyCard>(loyaltyCards.values());
			}
		}
		
	}
	
	
	@Override
	public LoyaltyCard findOne(Long id) {
		String sql = "SELECT * FROM loyaltycard where id = ?";
		LoyaltyCardRowCallBackHandler rowCallBackHandler = new LoyaltyCardRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		return rowCallBackHandler.getLoyalityCards().get(0);
	}
	
	@Override
	public LoyaltyCard findLoyaltyCardByUser(Long userId) {
		String sql = "SELECT * FROM loyaltycard where userId = ?";
		LoyaltyCardRowCallBackHandler rowCallBackHandler = new LoyaltyCardRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, userId);
		if (rowCallBackHandler.getLoyalityCards() != null) {
			return rowCallBackHandler.getLoyalityCards().get(0);
		} else {
			return null;
		}
		
	}

	@Override
	public List<LoyaltyCard> findAll() {
		String sql = "SELECT * FROM loyaltycard";
		LoyaltyCardRowCallBackHandler rowCallBackHandler = new LoyaltyCardRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getLoyalityCards();
	}

	@Override
	public int save(LoyaltyCard loyaltyCard) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into loyaltycard (userId, points, discount, approved, rejected) values (?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setLong(index++, loyaltyCard.getUserId());
				preparedStatement.setInt(index++, loyaltyCard.getPoints());
				preparedStatement.setInt(index++, loyaltyCard.getDiscount());
				preparedStatement.setBoolean(index++, loyaltyCard.isApproved());
				preparedStatement.setBoolean(index++, loyaltyCard.isRejected());
				
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public int update(LoyaltyCard loyaltyCard) {
		String sql = "update loyaltycard set userId = ?, points = ?, discount = ?, approved = ?, rejected = ? where userId = ?";
		boolean succses = jdbcTemplate.update(sql, loyaltyCard.getUserId(), loyaltyCard.getPoints(), loyaltyCard.getDiscount(), 
												loyaltyCard.isApproved(), loyaltyCard.isRejected(), loyaltyCard.getUserId()) == 1;
		return succses?1:0;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM loyaltycard WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

	@Override
	public List<LoyaltyCard> findLoyaltyCardsOnRequest() {
		String sql = "SELECT * FROM loyaltycard where approved = false and rejected = false";
		LoyaltyCardRowCallBackHandler rowCallBackHandler = new LoyaltyCardRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getLoyalityCards();
	}

	

}
