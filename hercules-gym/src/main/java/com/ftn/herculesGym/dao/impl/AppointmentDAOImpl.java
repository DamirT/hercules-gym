package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.AppointmentDAO;
import com.ftn.herculesGym.dao.HallDAO;
import com.ftn.herculesGym.dao.TrainingDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.TrainingType;
import com.ftn.herculesGym.model.User;

@Repository
public class AppointmentDAOImpl implements AppointmentDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TrainingDAO trainingDAO;
	
	@Autowired
	private HallDAO hallDAO;
	
	private class AppointmentRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Appointment> appointments = new LinkedHashMap<Long, Appointment>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			Long date_time_of_appoitment = rs.getLong(index++);
			Long hallId = rs.getLong(index++);
			Long trainingId = rs.getLong(index++);
			
			Hall hall = hallDAO.findOne(hallId);
			Training training = trainingDAO.findOne(trainingId);
			Appointment appointment = appointments.get(id);
			if(appointment == null) {
				appointment = new Appointment(id, training, hall, date_time_of_appoitment);
				appointments.put(id, appointment);
			}
		}
		
		public List<Appointment> getAppointments() {
			return new ArrayList<Appointment>(appointments.values());
		}
		
	}
	
	@Override
	public Appointment findOne(Long id) {
		String sql = "SELECT * FROM appointment where id = ?";
		AppointmentRowCallBackHandler rowCallBackHandler = new AppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);

		return rowCallBackHandler.getAppointments().get(0);
	}

	@Override
	public List<Appointment> findAll() {
		String sql = "SELECT * FROM appointment";
		AppointmentRowCallBackHandler rowCallBackHandler = new AppointmentRowCallBackHandler();
		
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getAppointments();
	}

	@Override
	public int save(Appointment appointment) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into appointment (date_time_of_appoitment, hallId, trainingId) values (?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setLong(index++, appointment.getDateAndTimeOfAppointment());
				preparedStatement.setLong(index++, appointment.getHall().getId());
				preparedStatement.setLong(index++, appointment.getTraining().getId());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public int update(Appointment appointment) {
		String sql = "update appointment set date_time_of_appoitment = ?, hallId = ?, trainingId = ? where id = ?";
		boolean succses = jdbcTemplate.update(sql, appointment.getDateAndTimeOfAppointment(), appointment.getHall().getId(), appointment.getTraining().getId(), appointment.getId()) == 1;
		return succses?1:0;
	}

	@Override
	public List<Appointment> findAllByTraining(Long trainingId) {
		String sql = "SELECT * FROM appointment where trainingId = ? order by id";
		AppointmentRowCallBackHandler rowCallBackHandler = new AppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, trainingId);
		return rowCallBackHandler.getAppointments();
	}

	@Override
	public List<Appointment> findAllByHallId(Long id) {
		String sql = "SELECT * FROM appointment where hallId = ? order by id";
		AppointmentRowCallBackHandler rowCallBackHandler = new AppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		return rowCallBackHandler.getAppointments();
	}

	@Override
	public int saveAppointmentsFromShoppinCart(User user, List<Appointment> appointments, LocalDateTime dateAndTimeOfOrder, double totalPricePerReservation) {
		boolean succses = false;
		String sql = "insert into userappointment (userId, appointmentId, date_and_time_of_order, total_price_per_reservation) values (?, ?, ?, ?)";
		for (Appointment a : appointments) {
			succses = jdbcTemplate.update(sql, user.getId(), a.getId(), dateAndTimeOfOrder, totalPricePerReservation) == 1;
		}
		return succses?1:0;
	}

	@Override
	public List<Appointment> findAllAppointmentsByUser(User user) {
		String sql = "SELECT * FROM appointment a"
				+ "	left join  userappointment ua"
				+ " on ua.appointmentId = a.id"
				+ " left join user u"
				+ " on u.id = ua.userId"
				+ " where u.id = ? order by date_time_of_appoitment desc";
		AppointmentRowCallBackHandler rowCallBackHandler = new AppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, user.getId());
		return rowCallBackHandler.getAppointments();
	}

	@Override
	public int deleteOrderedAppointment(User user, Appointment appointment) {
		String sql = "DELETE FROM userappointment WHERE userId = ? and appointmentId = ?";
		return jdbcTemplate.update(sql, user.getId(), appointment.getId());
	}
	
	@Override
	public List<Appointment> findAllAppointemntsByUserAndDateTime(User user, LocalDateTime ldt) {
		String sql = "SELECT * FROM appointment a"
				+ "	left join userappointment ua"
				+ "	on ua.appointmentId = a.id"
				+ "	left join user u"
				+ "	on u.id = ua.userId"
				+ "	where u.id = ? and ua.date_and_time_of_order = ? order by date_time_of_appoitment desc";
		AppointmentRowCallBackHandler rowCallBackHandler = new AppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, user.getId(), Timestamp.valueOf(ldt));
		return rowCallBackHandler.getAppointments();
	}

}
