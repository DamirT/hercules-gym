package com.ftn.herculesGym.dao;

import java.util.List;

import com.ftn.herculesGym.model.Hall;

public interface HallDAO {

	public Hall findOne(Long id);

	public List<Hall> findAll();
	
	public int save(Hall hall);
	
	public List<Hall> findAllByHallMark(String hallMark);

	public int update(Hall hall);
	
	public int delete(Long id);
}
