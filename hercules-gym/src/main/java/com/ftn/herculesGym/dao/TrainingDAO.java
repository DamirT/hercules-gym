package com.ftn.herculesGym.dao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;

public interface TrainingDAO {

	public Training findOne(Long id);
	
	public List<Training> findAll();
	
	public int update(Training training);
	
	public int save(Training training);
	
	public int delete(Long id);
	
	public List<Training> findByNameSearch(String text);
	
	public List<Training> findByCoachSearch(String text);
	
	public List<Training> findByPriceSearch(String from, String until);
	
	public int saveTrainingToUserWishList(User user, Training training);
	
	public List<Training> findAllByUserWishList(User user);
	
	public int deleteTrainingFromUserWishList(User user, Training training);
	
	public List<Training> findAllByAppointment(Appointment appointment);
	
	public Training findTrainingForComment(User user, Training training);
	
	public List<Training> findAllByAppointmentsBeetwenDates(Date from, Date until);
}
