package com.ftn.herculesGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.UserAppointmentDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;

@Repository
public class UserAppointmentDAOImpl implements UserAppointmentDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class UserAppointmentRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, UserAppointment> usersAppointments = new LinkedHashMap<Long, UserAppointment>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			Long userId = rs.getLong(index++);
			Long appointmentId = rs.getLong(index++);
			LocalDateTime dateAndTimeOfOrder = rs.getTimestamp(index++).toLocalDateTime();
			double totalPricePerReservation = rs.getDouble(index++);
			
			UserAppointment userAppointment = usersAppointments.get(id);
			if(userAppointment == null) {
				userAppointment = new UserAppointment(id, userId, appointmentId, dateAndTimeOfOrder, totalPricePerReservation);
				usersAppointments.put(id, userAppointment);
			}
		}
		
		public List<UserAppointment> getUsersAppointments() {
			return new ArrayList<UserAppointment>(usersAppointments.values());
		}
	}

	@Override
	public List<UserAppointment> findAllUserAppointments(User user) {
		String sql = "SELECT distinct ua.id, ua.userId, ua.appointmentId,  ua.date_and_time_of_order, ua.total_price_per_reservation FROM userappointment ua"
				+ "	left join appointment a"
				+ "	on ua.appointmentId = a.id"
				+ "	left join user u"
				+ "	on u.id = ua.userId"
				+ "	where u.id = ? group by ua.date_and_time_of_order order by ua.date_and_time_of_order desc";
		UserAppointmentRowCallBackHandler rowCallBackHandler = new UserAppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, user.getId());
		return rowCallBackHandler.getUsersAppointments();
	}

	@Override
	public UserAppointment findOne(Long id) {
		String sql = "SELECT * FROM userAppointment where id = ?";
		UserAppointmentRowCallBackHandler rowCallBackHandler = new UserAppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);

		return rowCallBackHandler.getUsersAppointments().get(0);
	}

	@Override
	public int updateUserappointemntWhenDelete(User user, LocalDateTime dateAndTimeOfReservation, double totalPricePerReservation) {
		String sql = "update userAppointment set total_price_per_reservation = ? where userId = ? and date_and_time_of_order = ?";
		boolean succses = jdbcTemplate.update(sql, totalPricePerReservation, user.getId(), dateAndTimeOfReservation) == 1;
		return succses?1:0;
	}

	@Override
	public UserAppointment findUserAppointmentByAppointmentAndUser(Long appointmentId, User user) {
		String sql = "select * from userappointment where appointmentId  = ? and userId = ?";
		UserAppointmentRowCallBackHandler rowCallBackHandler = new UserAppointmentRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, appointmentId, user.getId());

		return rowCallBackHandler.getUsersAppointments().get(0);
	}

}
