package com.ftn.herculesGym.dao;

import java.util.List;

import com.ftn.herculesGym.model.TrainingType;

public interface TrainingTypeDAO {

	public TrainingType findOne(Long id);

	public List<TrainingType> findAll();
	
}
