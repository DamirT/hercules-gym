package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.SpecialDateDiscountDAO;
import com.ftn.herculesGym.model.SpecialDateDiscount;

@Repository
public class SpecialDateDiscountDAOImpl implements SpecialDateDiscountDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class SpecialDateRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, SpecialDateDiscount> specialDatesOfDiscounts = new LinkedHashMap<Long, SpecialDateDiscount>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			LocalDate dateOfDiscounts = rs.getTimestamp(index++).toLocalDateTime().toLocalDate();
			int percentage = rs.getInt(index++);
			
			SpecialDateDiscount specialDateDiscount = specialDatesOfDiscounts.get(id);
			if(specialDateDiscount == null) {
				specialDateDiscount = new SpecialDateDiscount(id, dateOfDiscounts, percentage);
				specialDatesOfDiscounts.put(id, specialDateDiscount);
			}
		}
		
		public List<SpecialDateDiscount> getSpecialDaysDiscounts() {
			return new ArrayList<SpecialDateDiscount>(specialDatesOfDiscounts.values());
		}
		
	}
	
	
	@Override
	public int save(SpecialDateDiscount specialDateDiscount) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into specialDateDiscount (special_date_discount, discount_by_percentage) values (?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setDate(index++, Date.valueOf(specialDateDiscount.getDateOfDiscount()));
				preparedStatement.setInt(index++, specialDateDiscount.getPercentage());;

				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}


	@Override
	public List<SpecialDateDiscount> findAll() {
		String sql = "select * from specialDateDiscount where DATE(special_date_discount) >= curdate()";
		
		SpecialDateRowCallBackHandler rowCallBackHandler = new SpecialDateRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getSpecialDaysDiscounts();
	}

}
