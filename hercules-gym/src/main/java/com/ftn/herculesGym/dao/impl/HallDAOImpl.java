package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.HallDAO;
import com.ftn.herculesGym.model.Hall;

@Repository
public class HallDAOImpl implements HallDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class HallTypeRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Hall> halls = new LinkedHashMap<Long, Hall>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			int capacity = rs.getInt(index++);
			String hallMark = rs.getString(index++);
			boolean occupied = rs.getBoolean(index++);
			
			
			
			Hall hall = halls.get(id);
			if(hall == null) {
				hall = new Hall(id, capacity, hallMark);
				hall.setOccupied(occupied);
				halls.put(id, hall);
			}
		}
		
		public List<Hall> getHalls() {
			return new ArrayList<Hall>(halls.values());
		}
		
	}
	
	@Override
	public Hall findOne(Long id) {
		String sql = "SELECT * FROM hall where id = ?";
		HallTypeRowCallBackHandler rowCallBackHandler = new HallTypeRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);

		return rowCallBackHandler.getHalls().get(0);
	}

	@Override
	public List<Hall> findAll() {
		String sql = "SELECT * FROM hall";
		HallTypeRowCallBackHandler rowCallBackHandler = new HallTypeRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getHalls();
	}

	@Override
	public int save(Hall hall) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into hall (capacity, hall_mark, occupied) values (?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setInt(index++, hall.getCapacity());
				preparedStatement.setString(index++, hall.getHallMark());
				preparedStatement.setBoolean(index++, hall.isOccupied());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public List<Hall> findAllByHallMark(String hallMark) {
		String sql = "SELECT * FROM hall where hall_mark like ? \"%\" order by id";
		HallTypeRowCallBackHandler rowCallBackHandler = new HallTypeRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, hallMark);
		return rowCallBackHandler.getHalls();
	}

	@Override
	public int update(Hall hall) {
		String sql = "update hall set capacity = ?, hall_mark = ?, occupied = ? where id = ?";
		boolean succses = jdbcTemplate.update(sql, hall.getCapacity(), hall.getHallMark(), hall.isOccupied(), hall.getId()) == 1;
		return succses?1:0;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM hall WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}
}
