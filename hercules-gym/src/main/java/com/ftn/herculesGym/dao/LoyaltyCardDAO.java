package com.ftn.herculesGym.dao;

import java.util.List;

import com.ftn.herculesGym.model.Hall;
import com.ftn.herculesGym.model.LoyaltyCard;

public interface LoyaltyCardDAO {

	public LoyaltyCard findOne(Long id);
	
	public LoyaltyCard findLoyaltyCardByUser(Long userId);

	public List<LoyaltyCard> findAll();
	
	public int save(LoyaltyCard loyaltyCard);
	
	public int update(LoyaltyCard loyaltyCard);
	
	public int delete(Long id);
	
	public List<LoyaltyCard> findLoyaltyCardsOnRequest();
}
