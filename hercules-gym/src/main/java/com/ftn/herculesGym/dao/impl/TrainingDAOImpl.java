package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.TrainingDAO;
import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.LevelOfTraining;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.TrainingType;
import com.ftn.herculesGym.model.TypeOfTraining;
import com.ftn.herculesGym.model.User;

@Repository
public class TrainingDAOImpl implements TrainingDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class TrainingRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, Training> trainings = new LinkedHashMap<Long, Training>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String name = rs.getString(index++);
			String coach = rs.getString(index++);
			String description = rs.getString(index++);
			double price = rs.getDouble(index++);
			int duration = rs.getInt(index++);
			TypeOfTraining type = TypeOfTraining.valueOf(rs.getString(index++).toUpperCase());
			LevelOfTraining level = LevelOfTraining.valueOf(rs.getString(index++).toUpperCase());
			float avgAssesment = rs.getFloat(index++);
			
			Training training = trainings.get(id);
			if(training == null) {
				training = new Training(id, name, coach, description, price, duration, type, level);
				training.setAvgAssesment(avgAssesment);
				if (training != null) {
					trainings.put(id, training);
				}
			}
			
			if (training != null) {
				Long trainingTypeId = rs.getLong(index++);
				String title = rs.getString(index++);
				String detailDescription = rs.getString(index++);
				TrainingType trainingType = new TrainingType(trainingTypeId, title, detailDescription);
				training.getListOfTrainingsType().add(trainingType);
			}
		}
		
		public List<Training> getTrainings() {
			if (trainings == null || trainings.isEmpty()) {
				return null;
			} else {
				return new ArrayList<Training>(trainings.values());
			}
			
		}
		
	}
	
	@Override
	public Training findOne(Long id) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment, tt.id, tt.title, tt.detailDescription"  
					+ " FROM training t left join trainingtypetraining ttt on ttt.trainingId = t.id"
					+ " left join trainingtype tt on ttt.trainingTypeId = tt.id where t.id = ? order by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		
		if(rowCallBackHandler.getTrainings() == null) {
			return null;
		} else {
			return rowCallBackHandler.getTrainings().get(0);
		}
	}

	@Override
	public List<Training> findAll() {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment, tt.id, tt.title, tt.detailDescription"
					+ " FROM training t left join trainingtypetraining ttt on ttt.trainingId = t.id"
					+ " left join trainingtype tt on ttt.trainingTypeId = tt.id order by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getTrainings();
	}

	@Override
	public int save(Training training) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "insert into training (name, coach, description, price, duration, type_of_training, level_of_training, avg_assesment) values (?, ?, ?, ?, ?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, training.getName());
				preparedStatement.setString(index++, training.getCoach());
				preparedStatement.setString(index++, training.getDescription());
				preparedStatement.setDouble(index++, training.getPrice());
				preparedStatement.setInt(index++, training.getDuration());
				preparedStatement.setString(index++, training.getTypeOfTraining().getDisplayName());
				preparedStatement.setString(index++, training.getLevelOfTraining().getDisplayName());
				preparedStatement.setFloat(index++, training.getAvgAssesment());
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean succses = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		if (succses) {
			String sql = "insert into trainingtypetraining (trainingId, trainingTypeId) values (?, ?)";
			for (TrainingType tt : training.getListOfTrainingsType()) {
				succses = succses && jdbcTemplate.update(sql, keyHolder.getKey(), tt.getId()) == 1;
			}
		}
		return succses?1:0;
	}
	
	@Override
	public int update(Training training) {
		
		String sql = "delete from trainingtypetraining where trainingId = ?";
		jdbcTemplate.update(sql, training.getId());
		
		boolean succses = true;
		sql = "insert into trainingtypetraining (trainingId, trainingTypeId) values (?, ?)";
		for (TrainingType tt : training.getListOfTrainingsType()) {
			succses = succses && jdbcTemplate.update(sql, training.getId(), tt.getId()) == 1;
		}
		
		sql = "update training set name = ?, coach = ?, description = ?, price = ?, duration = ?, type_of_training = ?, level_of_training = ?, avg_assesment = ? where id = ?";
		String type = training.getTypeOfTraining().getDisplayName();
		String level = training.getLevelOfTraining().getDisplayName();
		succses = jdbcTemplate.update(sql, training.getName(), training.getCoach(), training.getDescription(), training.getPrice(), training.getDuration(), 
												type, level, training.getAvgAssesment(), training.getId()) == 1;
		
		return succses?1:0;
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Training> findByNameSearch(String text) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment, tt.id, tt.title, tt.detailDescription"
				+ " FROM training t left join trainingtypetraining ttt on ttt.trainingId = t.id"
				+ " left join trainingtype tt on ttt.trainingTypeId = tt.id where t.name like ? \"%\" order by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, text);
		return rowCallBackHandler.getTrainings();
	}

	@Override
	public List<Training> findByCoachSearch(String text) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment, tt.id, tt.title, tt.detailDescription"
				+ " FROM training t left join trainingtypetraining ttt on ttt.trainingId = t.id"
				+ " left join trainingtype tt on ttt.trainingTypeId = tt.id where t.coach like ? \"%\" order by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, text);
		return rowCallBackHandler.getTrainings();
	}

	@Override
	public List<Training> findByPriceSearch(String from, String until) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment, tt.id, tt.title, tt.detailDescription"
				+ " FROM training t left join trainingtypetraining ttt on ttt.trainingId = t.id"
				+ " left join trainingtype tt on ttt.trainingTypeId = tt.id where t.price between ? and ? order by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, from, until);   
		return rowCallBackHandler.getTrainings();
	}

	@Override
	public int saveTrainingToUserWishList(User user, Training training) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into userTraining (userId, trainingId) values (?, ?)";
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setLong(index++, user.getId());				
				preparedStatement.setLong(index++, training.getId());

				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public List<Training> findAllByUserWishList(User user) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment, tt.id, tt.title, tt.detailDescription"
				+ " FROM training t left join trainingtypetraining ttt on ttt.trainingId = t.id"
				+ " left join trainingtype tt on ttt.trainingTypeId = tt.id"
				+ " left join usertraining ut on ut.trainingId = t.id"
				+ " left join user u on ut.userId = u.id where u.id = ?";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, user.getId());
		return rowCallBackHandler.getTrainings();
	}

	@Override
	public int deleteTrainingFromUserWishList(User user, Training training) {
			String sql = "DELETE FROM usertraining WHERE userId = ? and trainingId = ?";
			return jdbcTemplate.update(sql, user.getId(), training.getId());
	}

	@Override
	public List<Training> findAllByAppointment(Appointment appointment) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment,"
				+ " tt.id, tt.title, tt.detailDescription from training t"
				+ " left join trainingtypetraining ttt on ttt.trainingId = t.id"
				+ " left join trainingtype tt on ttt.trainingTypeId = tt.id"
				+ " left join appointment a on t.id = a.trainingId where a.id = ? order by a.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, appointment.getId());
		return rowCallBackHandler.getTrainings();
	}

	@Override
	public Training findTrainingForComment(User user, Training training) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment,"
					+ "	tt.id, tt.title, tt.detailDescription from training t"
					+ " left join trainingtypetraining ttt on ttt.trainingId = t.id"
					+ " left join trainingtype tt on ttt.trainingTypeId = tt.id"
					+ " left join appointment a on t.id = a.trainingId"
					+ " left join userappointment ua on ua.appointmentId = a.id"
					+ " left join user u on ua.userId = u.id where u.id = ?"
					+ " and t.id = ?"
					+ " group by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, user.getId(), training.getId());
		if(rowCallBackHandler.getTrainings() == null) {
			return null;
		} else {
			return rowCallBackHandler.getTrainings().get(0);
		}
	}

	@Override
	public List<Training> findAllByAppointmentsBeetwenDates(Date from, Date until) {
		String sql = "SELECT t.id, t.name, t.coach, t.description, t.price, t.duration, t.type_of_training, t.level_of_training, t.avg_assesment,"
				+ "	tt.id, tt.title, tt.detailDescription from training t"
				+ "	left join trainingtypetraining ttt on ttt.trainingId = t.id"
				+ "	left join trainingtype tt on ttt.trainingTypeId = tt.id"
				+ "	left join appointment a on t.id = a.trainingId"
				+ "	left join userappointment ua on ua.appointmentId = a.id where DATE(ua.date_and_time_of_order)"
				+ " between ? and ? group by t.id";
		TrainingRowCallBackHandler rowCallBackHandler = new TrainingRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, from, until);
		return rowCallBackHandler.getTrainings();
	}
	
}
