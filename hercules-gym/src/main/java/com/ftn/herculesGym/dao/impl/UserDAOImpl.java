package com.ftn.herculesGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.UserDAO;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserRole;

@Repository
public class UserDAOImpl implements UserDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class UserRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, User> users = new LinkedHashMap<Long, User>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String userName = rs.getString(index++);
			String password = rs.getString(index++);
			String email = rs.getString(index++);
			String name = rs.getString(index++);
			String surName = rs.getString(index++);
			LocalDate dateBirth = rs.getTimestamp(index++).toLocalDateTime().toLocalDate();
			String adress = rs.getString(index++);
			String phoneNumber = rs.getString(index++);
			UserRole userRole = UserRole.valueOf(rs.getString(index++));
			LocalDateTime dateOfRegistration = rs.getTimestamp(index++).toLocalDateTime();
			boolean active = rs.getBoolean(index++);
			
			User user = users.get(id);
			if(user == null) {
				user = new User(id, name, surName, userName, password, email, adress, phoneNumber, dateBirth);
				user.setUserRole(userRole);
				user.setDateOfRegistration(dateOfRegistration);
				user.setActive(active);
				users.put(id, user);
			}
		}
		
		public List<User> getUsers() {
			return new ArrayList<User>(users.values());
		}
		
	}
	
	@Override
	public User findOne(Long id) {
		String sql = "select * from user where id = ?";
		
		UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		return rowCallBackHandler.getUsers().get(0);
	}

	@Override
	public User findOne(String userName, String password) {
		String sql = "select * from user where user_name = ? and password = ? order by id";
		
		UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, userName, password);
		
		if(rowCallBackHandler.getUsers().size() == 0) {
			return null;
		}
		
		return rowCallBackHandler.getUsers().get(0);
	}

	@Override
	public List<User> findAll() {
		String sql = "select * from user";
		
		UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getUsers();
	}

	@Override
	public int save(User user) {
			PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
				
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert into user (user_name, password, email, name, surname, date_of_birth, adress, phone_number, user_role, date_of_registration, active) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, user.getUserName());
				preparedStatement.setString(index++, user.getPassword());
				preparedStatement.setString(index++, user.getEmail());
				preparedStatement.setString(index++, user.getName());
				preparedStatement.setString(index++, user.getSurname());
				preparedStatement.setTimestamp(index++, Timestamp.valueOf(user.getDateBirth().atStartOfDay()));
				preparedStatement.setString(index++, user.getAdress());
				preparedStatement.setString(index++, user.getPhoneNumber());
				preparedStatement.setString(index++, user.getUserRole().toString());
				preparedStatement.setTimestamp(index++, Timestamp.valueOf(user.getDateOfRegistration()));
				preparedStatement.setBoolean(index++, user.isActive());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public List<User> findAllByUserNameSearch(String userName) {
		
		String sql = "select * from user where user_name like ? \"%\" ";
		UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, userName);
		return rowCallBackHandler.getUsers();
	}

	@Override
	public List<User> findAllByUserRoleSearch(String userRole) {
		
		String sql = "select * from user where user_role = ? ";
		UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, userRole);
		return rowCallBackHandler.getUsers();
	}

	@Override
	public int updateUser(User user) {
		String sql = "update user set user_name = ?, password = ?, email = ?, name = ?, surname = ?, date_of_birth = ?, adress = ?, "
					+ "phone_number = ?, user_role = ?, date_of_registration = ?, active = ? where id = ?";
		boolean succses = jdbcTemplate.update(sql, user.getUserName(), user.getPassword(), user.getEmail(), user.getName(), 
							user.getSurname(), user.getDateBirth(), user.getAdress(), user.getPhoneNumber(), user.getUserRole().toString(), user.getDateOfRegistration(), user.isActive(), user.getId()) == 1;
		return succses?1:0;
	}

	@Override
	public List<User> findAllByAppointmentId(Long appointmentd) {
		String sql = "SELECT * FROM user u"
				+ "	left join userappointment ua on ua.userId = u.id"
				+ "	left join appointment a on ua.appointmentId = a.id where a.id = ? order by u.id";
		UserRowCallBackHandler rowCallBackHandler = new UserRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, appointmentd);
		return rowCallBackHandler.getUsers();
	}

}
