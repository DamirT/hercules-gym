package com.ftn.herculesGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.herculesGym.dao.TrainingTypeDAO;
import com.ftn.herculesGym.model.LevelOfTraining;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.TrainingType;
import com.ftn.herculesGym.model.TypeOfTraining;

@Repository
public class TrainingTypeDAOImpl implements TrainingTypeDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class TrainingTypeRowCallBackHandler implements RowCallbackHandler {
		
		private Map<Long, TrainingType> trainingsTypes = new LinkedHashMap<Long, TrainingType>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String title = rs.getString(index++);
			String detailDesription = rs.getString(index++);
			
			
			
			TrainingType trainingType = trainingsTypes.get(id);
			if(trainingType == null) {
				trainingType = new TrainingType(id, title, detailDesription);
				trainingsTypes.put(id, trainingType);
			}
		}
		
		public List<TrainingType> getTrainingsTypes() {
			return new ArrayList<TrainingType>(trainingsTypes.values());
		}
		
	}
	
	@Override
	public TrainingType findOne(Long id) {
		String sql = "SELECT * FROM trainingType where id = ?";
		TrainingTypeRowCallBackHandler rowCallBackHandler = new TrainingTypeRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);

		return rowCallBackHandler.getTrainingsTypes().get(0);
	}

	@Override
	public List<TrainingType> findAll() {
		String sql = "SELECT * FROM trainingType";
		TrainingTypeRowCallBackHandler rowCallBackHandler = new TrainingTypeRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return rowCallBackHandler.getTrainingsTypes();
	}
	
}
