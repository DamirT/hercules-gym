package com.ftn.herculesGym.dao;

import java.time.LocalDateTime;
import java.util.List;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;
import com.ftn.herculesGym.model.UserAppointment;

public interface UserAppointmentDAO {

	UserAppointment findOne(Long id);
	
	List<UserAppointment> findAllUserAppointments(User user);

	int updateUserappointemntWhenDelete(User user, LocalDateTime dateAndTimeOfReservation, double totalPricePerReservation);
	
	UserAppointment findUserAppointmentByAppointmentAndUser(Long appointmentId, User user);
}
