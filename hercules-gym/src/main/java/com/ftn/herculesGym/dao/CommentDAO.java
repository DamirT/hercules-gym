package com.ftn.herculesGym.dao;

import java.util.List;

import com.ftn.herculesGym.model.Comment;
import com.ftn.herculesGym.model.Training;
import com.ftn.herculesGym.model.User;

public interface CommentDAO {

	public Comment findOne(Long id);
	
	public Comment findByUser(User user, Training training);

	public List<Comment> findAll();
	
	public int save(Comment comment);
	
	public int update(Comment comment);
	
	public int delete(Long id);
	
	public List<Comment> commentsForTraining(Training training);
	
	public List<Comment> findAllCommentsOnPending();
	
}
