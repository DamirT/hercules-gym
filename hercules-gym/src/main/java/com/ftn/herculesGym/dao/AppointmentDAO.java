package com.ftn.herculesGym.dao;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import com.ftn.herculesGym.model.Appointment;
import com.ftn.herculesGym.model.User;


public interface AppointmentDAO {

	public Appointment findOne(Long id);

	public List<Appointment> findAll();
	
	public int save(Appointment appointment);
	
	public int update(Appointment appointment);
	
	List<Appointment> findAllByTraining(Long id);
	
	List<Appointment> findAllByHallId(Long id);
	
	public int saveAppointmentsFromShoppinCart(User user, List<Appointment> appointments, LocalDateTime dateAndTimeOfOrder,  double totalPricePerReservation);
	
	List<Appointment> findAllAppointmentsByUser(User user);
	
	public int deleteOrderedAppointment(User user, Appointment appointment);
	
	public List<Appointment> findAllAppointemntsByUserAndDateTime(User user, LocalDateTime ldt);
}
