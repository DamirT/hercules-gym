package com.ftn.herculesGym.dao;

import java.util.List;

import com.ftn.herculesGym.model.SpecialDateDiscount;

public interface SpecialDateDiscountDAO {

	public int save (SpecialDateDiscount specialDateDiscount);
	
	public List<SpecialDateDiscount> findAll();
	
	
}
