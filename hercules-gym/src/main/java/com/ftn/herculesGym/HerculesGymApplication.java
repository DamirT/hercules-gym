package com.ftn.herculesGym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HerculesGymApplication {

	public static void main(String[] args) {
		SpringApplication.run(HerculesGymApplication.class, args);
	}

}
