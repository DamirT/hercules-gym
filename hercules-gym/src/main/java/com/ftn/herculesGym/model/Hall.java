package com.ftn.herculesGym.model;

public class Hall {

	private Long id;
	private int capacity;
	private String hallMark;
	private boolean occupied;
	
	public Hall(Long id, int capacity, String hallMark) {
		this.id = id;
		this.capacity = capacity;
		this.hallMark = hallMark;
		this.occupied = false;
	}

	public Hall(int capacity, String hallMark) {
		this.capacity = capacity;
		this.hallMark = hallMark;
		this.occupied = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getHallMark() {
		return hallMark;
	}

//	public void setHallMark(String hallMark) {
//		this.hallMark = hallMark;
//	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}
	
	
	
}
