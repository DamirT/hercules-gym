package com.ftn.herculesGym.model;

public class LoyaltyCard {

	private Long id;
	private Long userId;
	private int points;
	private int discount;
	private boolean approved;
	private boolean rejected;

	public LoyaltyCard(Long id, Long userId, int points, int discount, boolean approved, boolean rejected) {
		this.id = id;
		this.userId = userId;
		this.points = points;
		this.discount = discount;
		this.approved = approved;
		this.rejected = rejected;
	}

	public LoyaltyCard(Long userId, int points, int discount, boolean approved, boolean rejected) {
		this.userId = userId;
		this.points = points;
		this.discount = discount;
		this.approved = approved;
		this.rejected = rejected;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getDiscount() {
		return points * 5;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

}
