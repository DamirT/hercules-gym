package com.ftn.herculesGym.model;

public enum LevelOfTraining {

	AMATEUR("Amateur"),
	MEDIUM("Medium"),
	ADVANCED("Advanced");
	
	private final String displayName;

    private LevelOfTraining(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
	
}
