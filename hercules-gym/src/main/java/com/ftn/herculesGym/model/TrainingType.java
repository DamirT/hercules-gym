package com.ftn.herculesGym.model;

import java.util.List;

public class TrainingType {

	private Long id;
	private String title;
	private String detailDescription;
	private List<Training> trainings;
	
	public TrainingType(Long id, String title, String detailDescription, List<Training> trainings) {
		this.id = id;
		this.title = title;
		this.detailDescription = detailDescription;
		this.trainings = trainings;
	}

	public TrainingType(Long id, String title, String detailDescription) {
		super();
		this.id = id;
		this.title = title;
		this.detailDescription = detailDescription;
		this.trainings = null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetailDescription() {
		return detailDescription;
	}

	public void setDetailDescription(String detailDescription) {
		this.detailDescription = detailDescription;
	}

	public List<Training> getTrainings() {
		return trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}
	
}
