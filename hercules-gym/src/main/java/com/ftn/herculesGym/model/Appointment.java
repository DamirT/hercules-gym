package com.ftn.herculesGym.model;

public class Appointment {

	private Long id;
	private Training training;
	private Hall hall;
	private Long dateAndTimeOfAppointment;
	
	public Appointment(Long id, Training training, Hall hall, Long dateAndTimeOfAppointment) {
		this.id = id;
		this.training = training;
		this.hall = hall;
		this.dateAndTimeOfAppointment = dateAndTimeOfAppointment;
	}

	public Appointment(Training training, Hall hall, Long dateAndTimeOfAppointment) {
		this.training = training;
		this.hall = hall;
		this.dateAndTimeOfAppointment = dateAndTimeOfAppointment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public Hall getHall() {
		return hall;
	}

	public void setHall(Hall hall) {
		this.hall = hall;
	}

	public Long getDateAndTimeOfAppointment() {
		return dateAndTimeOfAppointment;
	}

	public void setDateAndTimeOfAppointment(Long dateAndTimeOfAppointment) {
		this.dateAndTimeOfAppointment = dateAndTimeOfAppointment;
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", training=" + training.getName() + ", hall=" + hall.getHallMark()  + ", dateAndTimeOfAppointment="
				+ dateAndTimeOfAppointment + "]";
	}
	
	
}
