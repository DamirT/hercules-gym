package com.ftn.herculesGym.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class User {
	
	private Long id;
	private String name;
	private String surname;
	private String userName;
	private String password;
	private String email;
	private String adress;
	private String phoneNumber;
	private LocalDate dateBirth;
	private UserRole userRole;
	private LocalDateTime dateOfRegistration;
	private boolean active;
	
	public User(Long id, String name, String surname, String userName, String password, String email, String adress,
			String phoneNumber, LocalDate dateBirth) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.adress = adress;
		this.phoneNumber = phoneNumber;
		this.dateBirth = dateBirth;
		this.userRole = null;
		this.dateOfRegistration = null;
		this.active = true;
	}

	public User(String name, String surname, String userName, String password, String email, String adress,
			String phoneNumber, LocalDate dateBirth) {
		super();
		this.name = name;
		this.surname = surname;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.adress = adress;
		this.phoneNumber = phoneNumber;
		this.dateBirth = dateBirth;
		this.userRole = null;
		this.dateOfRegistration = null;
		this.active = true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public LocalDate getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(LocalDate dateBirth) {
		this.dateBirth = dateBirth;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public LocalDateTime getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(LocalDateTime dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
