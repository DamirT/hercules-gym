package com.ftn.herculesGym.model;

import java.time.LocalDateTime;

public class UserAppointment {

	private Long id;
	private Long userId;
	private Long appointmentId;
	private LocalDateTime dateAndTimeOfOrder;
	private double totalPricePerReservation;
	

	
	public UserAppointment(Long userId, Long appointmentId, LocalDateTime dateAndTimeOfOrder,
			double totalPricePerReservation) {
		this.userId = userId;
		this.appointmentId = appointmentId;
		this.dateAndTimeOfOrder = dateAndTimeOfOrder;
		this.totalPricePerReservation = totalPricePerReservation;
	}

	public UserAppointment(Long id, Long userId, Long appointmentId, LocalDateTime dateAndTimeOfOrder,
			double totalPricePerReservation) {
		this.id = id;
		this.userId = userId;
		this.appointmentId = appointmentId;
		this.dateAndTimeOfOrder = dateAndTimeOfOrder;
		this.totalPricePerReservation = totalPricePerReservation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public LocalDateTime getDateAndTimeOfOrder() {
		return dateAndTimeOfOrder;
	}

	public void setDateAndTimeOfOrder(LocalDateTime dateAndTimeOfOrder) {
		this.dateAndTimeOfOrder = dateAndTimeOfOrder;
	}

	public double getTotalPricePerReservation() {
		return totalPricePerReservation;
	}

	public void setTotalPricePerReservation(double totalPricePerReservation) {
		this.totalPricePerReservation = totalPricePerReservation;
	}
	
}
