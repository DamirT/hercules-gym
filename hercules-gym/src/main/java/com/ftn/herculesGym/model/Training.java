package com.ftn.herculesGym.model;

import java.util.ArrayList;
import java.util.List;

public class Training {

	private Long id;
	private String name;
	private String coach;
	private String description;
	private double price;
	private int duration;
	private TypeOfTraining typeOfTraining;
	private LevelOfTraining levelOfTraining;
	private List<TrainingType> listOfTrainingsType = new ArrayList<TrainingType>();
	private float avgAssesment;

	public Training(Long id, String name, String coach, String description, double price, int duration, TypeOfTraining typeOfTraining, LevelOfTraining levelOfTraining) {
		this.id = id;
		this.name = name;
		this.coach = coach;
		this.description = description;
		this.price = price;
		this.duration = duration;
		this.typeOfTraining = typeOfTraining;
		this.levelOfTraining = levelOfTraining;
		this.avgAssesment = 0;
	}

	public Training(String name, String coach, String description, double price, int duration, TypeOfTraining typeOfTraining, LevelOfTraining levelOfTraining) {
		this.name = name;
		this.coach = coach;
		this.description = description;
		this.price = price;
		this.duration = duration;
		this.typeOfTraining = typeOfTraining;
		this.levelOfTraining = levelOfTraining;
		this.avgAssesment = 0;
	}
	
	public Training(String name, String coach, String description, double price, int duration) {
		this.name = name;
		this.coach = coach;
		this.description = description;
		this.price = price;
		this.duration = duration;
		this.avgAssesment = 0;
	}
	
	public Training(Long id, String name, String coach, String description, double price, int duration) {
		this.id = id;
		this.name = name;
		this.coach = coach;
		this.description = description;
		this.price = price;
		this.duration = duration;
		this.avgAssesment = 0;
	}
	
	public Training(Long id, String name, String coach, String description, double price, int duration,
			TypeOfTraining typeOfTraining, LevelOfTraining levelOfTraining, List<TrainingType> listOfTrainingsType) {
		this.id = id;
		this.name = name;
		this.coach = coach;
		this.description = description;
		this.price = price;
		this.duration = duration;
		this.typeOfTraining = typeOfTraining;
		this.levelOfTraining = levelOfTraining;
		this.listOfTrainingsType = listOfTrainingsType;
		this.avgAssesment = 0;
	}
	
	public Training(String name, String coach, String description, double price, int duration,
			TypeOfTraining typeOfTraining, LevelOfTraining levelOfTraining, List<TrainingType> listOfTrainingsType) {
		this.name = name;
		this.coach = coach;
		this.description = description;
		this.price = price;
		this.duration = duration;
		this.typeOfTraining = typeOfTraining;
		this.levelOfTraining = levelOfTraining;
		this.listOfTrainingsType = listOfTrainingsType;
		this.avgAssesment = 0;
	}

	public float getAvgAssesment() {
		return avgAssesment;
	}

	public void setAvgAssesment(float avgAssesment) {
		this.avgAssesment = avgAssesment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public TypeOfTraining getTypeOfTraining() {
		return typeOfTraining;
	}

	public void setTypeOfTraining(TypeOfTraining typeOfTraining) {
		this.typeOfTraining = typeOfTraining;
	}

	public LevelOfTraining getLevelOfTraining() {
		return levelOfTraining;
	}

	public void setLevelOfTraining(LevelOfTraining levelOfTraining) {
		this.levelOfTraining = levelOfTraining;
	}

	public List<TrainingType> getListOfTrainingsType() {
		return listOfTrainingsType;
	}

	public void setListOfTrainingsType(List<TrainingType> listOfTrainingsType) {
		this.listOfTrainingsType = listOfTrainingsType;
	}
	
}
