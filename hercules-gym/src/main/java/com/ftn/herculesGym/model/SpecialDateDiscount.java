package com.ftn.herculesGym.model;

import java.time.LocalDate;

public class SpecialDateDiscount {

	private Long id;
	private LocalDate dateOfDiscount;
	private int percentage;
	
	public SpecialDateDiscount(Long id, LocalDate dateOfDiscount, int percentage) {
		this.id = id;
		this.dateOfDiscount = dateOfDiscount;
		this.percentage = percentage;
	}

	public SpecialDateDiscount(LocalDate dateOfDiscount, int percentage) {
		this.dateOfDiscount = dateOfDiscount;
		this.percentage = percentage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDateOfDiscount() {
		return dateOfDiscount;
	}

	public void setDateOfDiscount(LocalDate dateOfDiscount) {
		this.dateOfDiscount = dateOfDiscount;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
	
}
