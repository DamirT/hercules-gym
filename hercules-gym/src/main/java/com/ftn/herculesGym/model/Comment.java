package com.ftn.herculesGym.model;

import java.time.LocalDate;

public class Comment {

	private Long id;
	private String textComment;
	private int assesment;
	private LocalDate dateOfPostedComment;
	private User user;
	private Long trainingId;
	private boolean approved;
	private boolean rejected;
	private boolean anonymous;

	public Comment(Long id, String textComment, int assesment, LocalDate dateOfPostedComment, User user,
			Long trainingId, boolean approved, boolean rejected, boolean anonymous) {
		this.id = id;
		this.textComment = textComment;
		this.assesment = assesment;
		this.dateOfPostedComment = dateOfPostedComment;
		this.user = user;
		this.trainingId = trainingId;
		this.approved = approved;
		this.rejected = rejected;
		this.anonymous = anonymous;
	}

	public Comment(String textComment, int assesment, LocalDate dateOfPostedComment, User user, Long trainingId,
			boolean approved, boolean rejected, boolean anonymous) {
		this.textComment = textComment;
		this.assesment = assesment;
		this.dateOfPostedComment = dateOfPostedComment;
		this.user = user;
		this.trainingId = trainingId;
		this.approved = approved;
		this.rejected = rejected;
		this.anonymous = anonymous;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextComment() {
		return textComment;
	}

	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}

	public int getAssesment() {
		return assesment;
	}

	public void setAssesment(int assesment) {
		this.assesment = assesment;
	}

	public LocalDate getDateOfPostedComment() {
		return dateOfPostedComment;
	}

	public void setDateOfPostedComment(LocalDate dateOfPostedComment) {
		this.dateOfPostedComment = dateOfPostedComment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

}
