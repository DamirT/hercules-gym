package com.ftn.herculesGym.model;

public enum TypeOfTraining {

	INDIVIDUAL("Individual"),
	GROUP("Group");
	
	private final String displayName;

    private TypeOfTraining(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
